

import sys
import codecs
import urllib
from urllib.error import HTTPError
import urllib.request
from bs4 import BeautifulSoup
card_csv_path="gwent_data.csv"
leader_csv_path="leader_data.csv"
carddata=["Name;Kraft;Tag;Place;Faction;"]
leaderdata=["Name;Faction;"]
base_url = "https://witcher.fandom.com/wiki/"
special_url = "Gwent_special_cards"
types=(("Gwent_close_combat_cards","COMBAT","MELEE"),
            ("Gwent_ranged_combat_cards","COMBAT","RANGED"),
            ("Gwent_siege_cards","COMBAT","ARTILLERY"),
            ("Gwent_agile_cards","AGILE"),
            ("Gwent_hero_cards","HERO"),
            ("Gwent_medic_cards","MEDIC"),
            ("Gwent_morale_boost_cards","MORALEBOOST"),
            ("Gwent_muster_cards","MUSTER"),
            ("Gwent_spy_cards","SPY"),
            ("Gwent_tight_bond_cards","TIGHTBOND"),
            ("Gwent_weather_cards","WEATHER"))
specialtypes =("BERSERKER","HORNBOOST","DECOY","MARDROME","SCORCH","SUMMONAVANGER")
factions= (("Monsters_Gwent_deck","MONSTER"),
("Nilfgaardian_Empire_Gwent_deck","NILFGAARDIAN_EMPIRE"),
("Northern_Realms_Gwent_deck","NORTHERN_REALMS"),
("Scoia'tael_Gwent_deck","SCOIATAEL"),
("Skellige_Gwent_deck","SKELLIGE"),
("Gwent_neutral_cards","NEUTRAL"))
            


    





def catch_404(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except HTTPError as e:
            
                
                
            raise e
    return wrapper

def get_attr_or_empty(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except AttributeError:
            return "Not found"
    return wrapper



@catch_404
def get_and_soupify(subdir):
    url = (base_url + subdir)
    url = urllib.parse.quote_plus(url.encode('utf8'), '/:')
    response = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(response, 'html.parser')
    return soup

def add_leader(name,faction):
    nameraw=name
    name = name.strip().replace(" ","_").replace(":","").replace("-","").replace("'","")
    leaderdata.append(name+";"+faction+";"+get_describtion(nameraw)+";")
def add_to_csv(name,tag=None,power=None,place=None,faction=None):
    nameraw=name
    name = name.strip().replace(" ","_").replace(":","").replace("-","").replace("'","")
    data=carddata

    for line in data:
        if line.split(";")[0]==name:
            
            a = line.split(";")
            if not len(a)==6:
                print('ok')
            if not power==None:
                a[1] = power
            if not tag==None and a[2].find(tag)==-1:
                a[2] +=","+tag
            if not place==None and a[3].find(place)==-1:
                a[3] +=","+place
            if not faction==None:
                a[4] =faction
            b=''
            a.pop(len(a)-1)
            for c in a:
                b+=c+";"
            data[data.index(line)] = b
            return
    a=name+";"
    if not power==None:
        a += power+";"
    else:
        a +=";"
    if not tag==None:
        a+=tag+";"
    else:
        a +=";"
    
    if not place==None:
        a+=place+";"
    else:
        a +=";"
    if not faction==None:
        a+=faction+";"
    else:
        a +=";"
    a+=get_describtion(nameraw)+";"
    data.append(a)

def get_describtion(name):
    path = name.replace(' ','_')+'_(gwent_card)'
    soup = get_and_soupify(path)
    desc = soup.find('figcaption').text.strip()
    if desc=='D&D Beyond':
        return ''
    else:
        return desc
    
def main():

    
    
    #normal
    for x in types:

        soup = get_and_soupify(x[0])
        
        table = soup.find('table')
        #table_body = table.find('tbody')

        rows = table.find_all('tr')
        for row in rows:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
                
            if len(cols) > 0 and not cols[2]=='Leader' and not cols[1].endswith(')'):
                
                if x[1] == "COMBAT":
                    add_to_csv(cols[1],tag=x[1],power=cols[3],place=x[2])
                else:
                    add_to_csv(cols[1],tag=x[1])

    #special
    soup = get_and_soupify(special_url)
    index=0
    for table in soup.find_all('table'):
        
        rows = table.find_all('tr')
        for row in rows:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            if len(cols) > 0 and not cols[2]=='Leader':
                add_to_csv(cols[1],tag=specialtypes[index])
        index+=1
        if index >5:
            break

    #faction
    for x in factions:
        soup = get_and_soupify(x[0])

        table = soup.find('table')
        #table_body = table.find('tbody')

        rows = table.find_all('tr')
        for row in rows:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            if len(cols) > 0 :
                if  not cols[2]=='Leader':
                    add_to_csv(cols[1],faction=x[1])
                else:
                    add_leader(cols[1],x[1])

    with codecs.open(card_csv_path,'w', encoding='utf8') as file:
        for line in carddata:
            file.write(line+"\n")

    with codecs.open(leader_csv_path,'w',encoding='utf8') as file:
        for line in leaderdata:
            file.write(line+"\n")

    

if __name__ == "__main__":
    main()