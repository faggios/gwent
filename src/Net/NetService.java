package Net;

import model.Game;

import java.net.Socket;

public interface NetService {
    void sendObject(Object object);

    Socket getClient();

    Object receiveObject();

    void stop();
}
