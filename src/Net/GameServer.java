package Net;

import controller.MainController;
import controller.MultiplayerController;
import model.Game;
import view.lobby.LobbyController;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class GameServer implements NetService {

    ServerSocket serverSocket;
    Socket client;
    Thread thread;
    LobbyController lobby;
    MainController mainController;
    ObjectOutputStream streamOut;
    ObjectInputStream streamIn;
    public GameServer(LobbyController lobby, MainController mainController) throws IOException {
        this.lobby = lobby;
        this.mainController = mainController;
        streamOut=null;
        streamIn=null;

        serverSocket = new ServerSocket(MultiplayerController.PORTNUM);
        client = null;
        thread = new ThreadStuff(serverSocket);
        thread.start();
    }

    public Socket getClient() {
        return client;
    }

    public void setClient(Socket client) {
        System.out.println("Client joined!");
        this.client = client;
        try{
            streamOut = new ObjectOutputStream(client.getOutputStream());
            streamIn = new ObjectInputStream(client.getInputStream());
        }catch(IOException e){
            System.out.println("Error: couldnt set input&output streams");
        }
        lobby.playerJoined((String) receiveObject());
        System.out.println("Playername received, now sending our playername: "+lobby.getPlayername());
        sendObject(lobby.getPlayername());
    }

    public void stop() {
        if (thread != null) thread.interrupt();
        try {
            streamIn.close();
            streamOut.close();
            if (client != null) {
                client.close();
                client = null;
            }
            serverSocket.close();
            System.out.println("Server closed!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ThreadStuff extends Thread {
        private ServerSocket serverSocket;

        public ThreadStuff(ServerSocket serversocket) {
            this.serverSocket = serversocket;
        }

        public void run() {
            try {
                System.out.println("Now waiting for client");
                Socket c = serverSocket.accept();
                System.out.println("Client found");
                setClient(c);
            }catch(SocketException e){
                System.out.println("Client joined successfully!");
                //good, bye bye socket
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Error waiting for client");
            }
        }
    }

    @Override
    public void sendObject(Object object) {
        if (client == null) {
            System.out.println("Error: No 2nd players has joined yet");
            return;
        }else if(streamOut == null){
            System.out.println("Error: output stream is null");
            return;
        }
        try {
            streamOut.writeObject(object);
            System.out.println("Object has been send");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error sending Object");
        }
    }

    @Override
    public Object receiveObject() {
        try {
            Object o = streamIn.readObject();
            System.out.println("Object has been received");
            return o;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Error receiving object");
            return null;
        }
    }
}
