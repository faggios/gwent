package Net;

import controller.MainController;
import controller.MultiplayerController;
import view.lobby.LobbyController;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class GameClient implements NetService {
    Socket client;
    ObjectInputStream streamIn;
    ObjectOutputStream streamOut;

    public GameClient(LobbyController lobby, MainController mainController, String host) throws IOException {

        client = new Socket(host, MultiplayerController.PORTNUM);
        if (client.isConnected()&&client.isBound()) {
            streamIn = new ObjectInputStream(client.getInputStream());
            streamOut = new ObjectOutputStream(client.getOutputStream());
        }else System.out.println("Error connecting to the server, socket could not be initiated");

    }

    public Socket getClient() {
        return client;
    }

    public void stop() {
        try {
            client.close();
            streamOut.close();
            streamIn.close();
            System.out.println("Closed connection");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendObject(Object object) {
        if (client == null)
            System.out.println("Error: You didnt join a game yet");
        try {
            streamOut.writeObject(object);
            System.out.println("Object has been sent");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error sending object");
        }
    }

    @Override
    public Object receiveObject() {
        try {
            Object o = streamIn.readObject();
            System.out.println("Object has been received");
            return o;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Error receiving object");
            return null;
        }
    }
}
