package controller;

import javafx.scene.layout.StackPane;
import model.Gwent;
import view.gameboard.GameBoardController;

public class MainController {

    private IOController ioController;
    private  GameController gameController;
    private  PlayerController playerController;
    private EffectController effectController;
    private DeckController deckController;
    private PointController pointController;
    private MultiplayerController multiplayerController;
    private LeaderEffectController leaderEffectController;
    private GameBoardController gameBoardController;

    private Gwent gwent;

    private StackPane stackPane;

    public MainController(StackPane stackPane){
        ioController = new IOController(this);
        gameController = new GameController(this);
        playerController = new PlayerController(this);
        effectController = new EffectController(this);
        deckController = new DeckController(this);
        pointController = new PointController(this);
        multiplayerController = new MultiplayerController(this);
        leaderEffectController = new LeaderEffectController(this);

        gwent = ioController.load();

    }
    public void setGameBoardController(GameBoardController gameBoardController){
        this.gameBoardController = gameBoardController;
    }

    public GameBoardController getGameBoardController() { return gameBoardController; }

    public LeaderEffectController getLeaderEffectController() { return leaderEffectController; }

    public MultiplayerController getMultiplayerController() { return multiplayerController; }

    public StackPane getStackPane() { return stackPane; }

    public void setStackPane(StackPane stackPane) { this.stackPane = stackPane; }

    public PointController getPointController() { return pointController; }

    public DeckController getDeckController() { return deckController; }

    public IOController getIoController() {
        return ioController;
    }

    public GameController getGameController() {
        return gameController;
    }

    public PlayerController getPlayerController() {
        return playerController;
    }

    public EffectController getEffectController() {
        return effectController;
    }

    public Gwent getGwent() {
        return gwent;
    }

}
