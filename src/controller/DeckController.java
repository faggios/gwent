package controller;

import model.Card;
import model.Deck;
import model.Faction;
import model.Leader;

import java.util.List;
import java.util.Map;

public class DeckController {

    private MainController mainController;

    public DeckController(MainController mainController) {
        this.mainController = mainController;
    }

    public Deck createDeck(String deckname, List<Card> cards, Leader leader, Faction faction) {
        for(Deck deck: mainController.getGwent().getDecks()){
            if(deck.getName().equals(deckname))
                return null;
        }
        Deck deck = new Deck();
        deck.setDeckName(deckname);
        deck.setCards(cards);
        deck.setLeader(leader);
        deck.setFaction(faction);
        mainController.getGwent().getDecks().add(deck);
        return deck;
    }

    public Deck deleteDeck(Deck deck) {
        mainController.getGwent().getDecks().remove(deck);
        return deck;
    }

    public Deck deleteDeck(String deckName) {
        Deck removeDeck = null;
        for (Deck deck : mainController.getGwent().getDecks()) {
            if (deck.getName().equals(deckName)) {
                removeDeck = deck;
                break;
            }
        }
        mainController.getGwent().getDecks().remove(removeDeck);
        return removeDeck;
    }
}
