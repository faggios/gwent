package controller;

import model.*;
import org.javatuples.Pair;
import view.endScreen.EndScreenController;
import view.gameboard.GameBoardController;
import view.lobby.LobbyController;
import view.scojatelFaction.ScojatelFactionController;
import view.selectCardView.SelectCardController;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GameController {

    private Game newGameG;
    private MainController mainController;

    public GameController(MainController mainController) {
        this.mainController = mainController;
    }

    public Game getGame() {
        return mainController.getGwent().getGame();
    }


    public void endTurn() { //TODO update Card-Punkte vorm game schicken
        if(MultiplayerController.isHost){
            if(getGame().getCurrentPlayer()==1){
                mulc().send(getGame());
            }
            else if (getGame().getCurrentPlayer()==2){
                mainController.getGwent().setGame(mulc().recieveGame());
            }
        }
        else {
            if(getGame().getCurrentPlayer()==2){
                mulc().send(getGame());
            }
            else if (getGame().getCurrentPlayer()==1){
                mainController.getGwent().setGame(mulc().recieveGame());
            }
        }
        nextPlayer();

    }

    public void startTurn() { // brauchen wir aktuell noch nicht

    }

    /**
     * Takes a lifepoint of the player with less combatPoints
     * and if at least one player has lifePoints == 0: ends the game
     */
    public void endRound() { //TODO in Game den ++gewinner++ + score eintragen

        int score1 = poc().calculateCombatPoints(p1());
        int score2 = poc().calculateCombatPoints(p2());
        if (score1 > score2) {
            plc().reduceLifePoints(p2());
            if(p1().getFaction().getName()==FactionName.NORTHERN_REALMS){
                plc().drawNewCard(p1(),1);
            }
            getGame().getWinnerMap().put(getGame().getRound(),1);
        } else if (score1 < score2) {
            plc().reduceLifePoints(p1());
            if(p2().getFaction().getName()==FactionName.NORTHERN_REALMS){
                plc().drawNewCard(p2(),1);
            }
            getGame().getWinnerMap().put(getGame().getRound(),2);
        } else {
            if(p1().getFaction().getName()==FactionName.NILFGAARDIAN_EMPIRE | p2().getFaction().getName()!=FactionName.NILFGAARDIAN_EMPIRE){
                plc().reduceLifePoints(p2());
            }

            if(p2().getFaction().getName()==FactionName.NILFGAARDIAN_EMPIRE | p1().getFaction().getName()!=FactionName.NILFGAARDIAN_EMPIRE){
                plc().reduceLifePoints(p1());
            }
            getGame().getWinnerMap().put(getGame().getRound(),0);

        }

        getGame().getScoreMap().put(getGame().getRound(),new Pair<>(score1,score2));

        if (plc().isDead(p1()) | plc().isDead(p2())) {
            endGame();
        } else {
            if(getGame().getRound().equals(Round.ONE))getGame().setRound(Round.TWO);
            else if (getGame().getRound().equals(Round.TWO))getGame().setRound(Round.THREE);
            clearWeather();
            //TODO clear rows
            //beachte monster effeckt hier//TODO hier auch multiplayer beachten
            //einfach karte rüberschicken
            startRound();
        }

    }

    public void startRound() {  //TODO welcher spieler drann?
        startTurn();

    }

    public void startGame(Player player1,Player player2,Game newGame) {
        if(player1.getFaction().getName()==FactionName.SCOIATAEL && player2.getFaction().getName()!=FactionName.SCOIATAEL) {
            System.out.println("Player 1 can choose who starts due to Scoiatel");
            if (MultiplayerController.isHost) {
                mainController.getStackPane().getChildren().add(new ScojatelFactionController(mainController.getStackPane(),mainController,newGame));
            } else {
                System.out.println("Waiting for who starts");
                newGame.setCurrentPlayer(mulc().recieveInt());
            }
        }
        else if(player2.getFaction().getName()==FactionName.SCOIATAEL && player1.getFaction().getName()!=FactionName.SCOIATAEL) {
            System.out.println("Player 2 can choose who starts due to Scoiatel");
            if (!MultiplayerController.isHost) {
                mainController.getStackPane().getChildren().add(new ScojatelFactionController(mainController.getStackPane(), mainController, newGame));
            } else {
                System.out.println("Waiting for who starts");
                newGame.setCurrentPlayer(mulc().recieveInt());
            }
        }
        else {
            if (MultiplayerController.isHost) {
                newGame.setCurrentPlayer(new Random().nextInt(2) + 1);
                System.out.println("Starting player has been chosen as: "+newGame.getCurrentPlayer());
                System.out.println("Sending the starting player int");
                mulc().send(newGame.getCurrentPlayer());
                System.out.println("starting player int send");
            } else {
                System.out.println("Receiving the starting player int");
                newGame.setCurrentPlayer(mulc().recieveInt());
                System.out.println("starting player int received");
            }
        }


        if(p1().getLeader().getName().equals(LeaderName.Emhyr_var_Emreis_The_White_Flame)||p2().getLeader().getName().equals(LeaderName.Emhyr_var_Emreis_The_White_Flame)){
            System.out.println("Leader not playable due to Emhyr White Flame");
            p1().getLeader().setUsed(true);
            p2().getLeader().setUsed(true);
        }

        Collections.shuffle(myplayer().getDiscardPile());



        if(myplayer().getLeader().getName().equals(LeaderName.Francesca_Findabair_Daisy_of_the_Valley)&&!myplayer().getLeader().isUsed())
            plc().drawNewCard(myplayer(),11);
        else
            plc().drawNewCard(myplayer(),10);

        mainController.getGwent().getGame().setRound(Round.ONE);
        //lasse 2 mal tauschen
        this.newGameG = newGame;
        System.out.println("Now selecting cards to replace");
        mainController.getStackPane().getChildren().add(new SelectCardController(mainController.getStackPane(),mainController,6,myplayer(),myplayer().getHandcards(),false,null));
    }

    public void continueStartGame(){

        if(MultiplayerController.isHost){
            System.out.println("Receiving the client player");
            newGameG.setPlayer2(mulc().recievePlayer());

            System.out.println("client player received, now sending the game");
            mulc().send(newGameG);
            System.out.println("Game send");
            mainController.getGwent().setGame(newGameG);
        }
        else{
            System.out.println("Sending the client player");
            mulc().send(myplayer());

            System.out.println("client player send, now Receiving the game");
            mainController.getGwent().setGame(mulc().recieveGame());
            System.out.println("game received");
        }
        System.out.println("Game is set to play");

        mainController.getGameBoardController().toggleLock();
        //starte runde
        startRound();
    }

    public void endGame() { //TODO view anzeigen + mulitplayer server disconnect
        if(getGame().getPlayer1().getLifePoints() == 0||getGame().getPlayer2().getLifePoints() == 0) {
            getGame().setRound(Round.END);
            mainController.getStackPane().getChildren().add(new EndScreenController(mainController.getStackPane(), mainController));
            mulc().stop();
        }else
            System.out.println("Error: Game hasn't ended yet.");


    }

    @Deprecated
    public void playCard(Place place, Card card){
        if(!myplayer().getHandcards().contains(card))
            throw new IllegalStateException("Ausgewählte karte ist nicht in Hand verhanden");
        EffectController efc = mainController.getEffectController();
        efc.doEffect(card,myplayer(),null,place);//TODO genau mit flo absprechen,nochmal überdenken

        endTurn();

    }

    public void playLeader(){
        if(myplayer().getLeader().isUsed())
            throw new IllegalStateException("Leader wurde schon verwendet");
        LeaderEffectController lefc = mainController.getLeaderEffectController();
        lefc.doEffect(myplayer().getLeader(),myplayer(),null,null);//TODO genau überdenken
        myplayer().getLeader().setUsed(true);

    }

    /**
     * Switches the currentPlayer to the next player and returns the new player
     */
    private void nextPlayer() {
        getGame().setCurrentPlayer(getGame().getCurrentPlayer() == 1 ? 2 : 1);
    }

    public Player getCurrentPlayer() {
        if (getGame() != null)
            return getGame().getCurrentPlayer() == 1 ? getGame().getPlayer1() : getGame().getPlayer2();
        return null;
    }


    private Player p1(){
        return getGame().getPlayer1();
    }

    private Player p2(){
        return getGame().getPlayer2();
    }

    private PlayerController plc(){
        return mainController.getPlayerController();
    }

    public Player myplayer(){
        return MultiplayerController.isHost?p1():p2();
    }
    private PointController poc(){
        return mainController.getPointController();
    }
    private MultiplayerController mulc(){
        return mainController.getMultiplayerController();
    }
    private void clearWeather(){
        mainController.getEffectController().clearweatherEffect(null,null);
    }
}
