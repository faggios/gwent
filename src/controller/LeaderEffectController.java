package controller;

import model.*;
import view.selectCardView.SelectCardController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class LeaderEffectController {

    private MainController mainController;

    public LeaderEffectController(MainController mainController) {
        this.mainController = mainController;
    }

    public void doEffect(Leader leader, Player player, Card pointedCard, Place place) {
        switch (leader.getName()) {
            case Eredin_Breacc_Glas_The_Treacherous: eredinGlass(leader, player, pointedCard, place);break;
            case Eredin_Bringer_of_Death: eredinDeath(leader, player, pointedCard, place);break;
            case Eredin_Commander_of_the_Red_Riders: eredinRed(leader, player, pointedCard, place);break;
            case Eredin_Destroyer_of_Worlds: eredinWorld(leader, player, pointedCard, place);break;
            case Eredin_King_of_the_Wild_Hunt: eredinKing(leader, player, pointedCard, place);break;
            case Emhyr_var_Emreis_Emperor_of_Nilfgaard: emhyrEmperor(leader, player, pointedCard, place);break;
            case Emhyr_var_Emreis_His_Imperial_Majesty: emhyrMajesty(leader, player, pointedCard, place);break;
            case Emhyr_var_Emreis_Invader_of_the_North: emhyrInvader(leader, player, pointedCard, place);break;
            case Emhyr_var_Emreis_The_Relentless: emhyrRelentless(leader, player, pointedCard, place);break;
            case Emhyr_var_Emreis_The_White_Flame: emhyrFlame(leader, player, pointedCard, place);break;
            case Foltest_King_of_Temeria: foltestKing(leader, player, pointedCard, place);break;
            case Foltest_Lord_Commander_of_the_North: foltestLord(leader, player, pointedCard, place);break;
            case Foltest_Son_of_Medell: foltestSon(leader, player, pointedCard, place);break;
            case Foltest_The_Siegemaster: foltestSiege(leader, player, pointedCard, place);break;
            case Foltest_The_SteelForged: foltestSteel(leader, player, pointedCard, place);break;
            case Francesca_Findabair_Daisy_of_the_Valley: francescaDaisy(leader, player, pointedCard, place);break;
            case Francesca_Findabair_Hope_of_the_Aen_Seidhe: francescaHope(leader, player, pointedCard, place);break;
            case Francesca_Findabair_Pureblood_Elf: francescaElf(leader, player, pointedCard, place);break;
            case Francesca_Findabair_Queen_of_Dol_Blathanna: francescaQueen(leader, player, pointedCard, place);break;
            case Francesca_Findabair_The_Beautiful: francescaBeautiful(leader, player, pointedCard, place);break;
            case Crach_an_Craite: crach(leader, player, pointedCard, place);break;
            case King_Bran: kingBran(leader, player, pointedCard, place);break;
        }
    }

    void eredinGlass(Leader leader, Player player, Card pointedCard, Place place){

    }

    void eredinDeath(Leader leader, Player player, Card pointedCard, Place place){
        mainController.getStackPane().getChildren().add(new SelectCardController(mainController.getStackPane(),mainController,2,player,player.getDiscardPile(),false,null));
    }

    void eredinRed(Leader leader, Player player, Card pointedCard, Place place){
        if(!player.getBoostType().get(Place.MELEE).equals(BoostType.NORMAL)){
            mainController.getPlayerController().playCard(player,player,Card.buildCard(CardName.Commanders_Horn),null,Place.MELEE,null,false);
        }
    }

    void eredinWorld(Leader leader, Player player, Card pointedCard, Place place){ //list = handcards,list2=discard,list3=draw
        mainController.getStackPane().getChildren().add(new SelectCardController(mainController.getStackPane(),mainController,5,player,player.getHandcards(),false,null));
    }

    void eredinKing(Leader leader, Player player, Card pointedCard, Place place){
        mainController.getStackPane().getChildren().add(new SelectCardController(mainController.getStackPane(),mainController,1,player,player.getDrawPile(),false,null));
    }

    void emhyrEmperor(Leader leader, Player player, Card pointedCard, Place place){
        mainController.getStackPane().getChildren().add(new SelectCardController(mainController.getStackPane(),mainController,5,player,null,false,null));
    }

    void emhyrMajesty(Leader leader, Player player, Card pointedCard, Place place){
        for(Card c : player.getDrawPile()){
            if(c.getCardName().equals(CardName.Torrential_Rain)){
                mainController.getPlayerController().playCard(player,player,c,null,Place.ARTILLERY,player.getDrawPile(),false);
                return;
            }
        }
    }
    void emhyrInvader(Leader leader, Player player, Card pointedCard, Place place){
        if(mainController.getGwent().getGame().getPlayer1().getDiscardPile().size()>0) {
            Card play = mainController.getGwent().getGame().getPlayer1().getDiscardPile().get(new Random().nextInt(mainController.getGwent().getGame().getPlayer1().getDiscardPile().size()));
            if (play != null){ //TODO
                //mainController.getPlayerController().playCard(mainController.getGwent().getGame().getPlayer1(),play.getTypes().contains(EffectType.SPY)?mainController.getGwent().getGame().getPlayer2():mainController.getGwent().getGame().getPlayer1(),play,);
            }
        }
        if(mainController.getGwent().getGame().getPlayer2().getDiscardPile().size()>0) {
            Card play = mainController.getGwent().getGame().getPlayer2().getDiscardPile().get(new Random().nextInt(mainController.getGwent().getGame().getPlayer2().getDiscardPile().size()));
            if (play != null){ //TODO
                //mainController.getPlayerController().playCard(mainController.getGwent().getGame().getPlayer2(),play.getTypes().contains(EffectType.SPY)?mainController.getGwent().getGame().getPlayer1():mainController.getGwent().getGame().getPlayer2(),play,);
            }
        }

    }
    void emhyrRelentless(Leader leader, Player player, Card pointedCard, Place place){
        mainController.getStackPane().getChildren().add(new SelectCardController(mainController.getStackPane(),mainController,4,player,mainController.getPlayerController().getOtherPlayer(player).getDiscardPile(),false,null));
    }
    void emhyrFlame(Leader leader, Player player, Card pointedCard, Place place){

    }
    void foltestKing(Leader leader, Player player, Card pointedCard, Place place){
        for(Card c : player.getDrawPile()){
            if(c.getCardName().equals(CardName.Impenetrable_Fog)){
                mainController.getPlayerController().playCard(player,player,c,null,Place.RANGED,player.getDrawPile(),false);
                return;
            }
        }
    }
    void foltestLord(Leader leader, Player player, Card pointedCard, Place place){
        mainController.getPlayerController().playCard(player,player,Card.buildCard(CardName.Clear_Weather),null,Place.MELEE,null,false);
    }
    void foltestSon(Leader leader, Player player, Card pointedCard, Place place){
        Player otherPlayer = mainController.getPlayerController().getOtherPlayer(player);
        if(mainController.getPointController().calculateCombatPoints(otherPlayer)>=10){
            Card remove = null;
            int score = -1;
            for(Card c : otherPlayer.getCardListPlace(Place.RANGED)){
                if(c.getPoints()>score&&!c.getTypes().contains(EffectType.HERO)) {
                    remove = c;
                    score = c.getPoints();
                }
            }
            if(remove != null){
                otherPlayer.getCardListPlace(Place.RANGED).remove(remove);
            }
        }
    }
    void foltestSiege(Leader leader, Player player, Card pointedCard, Place place){
        if(!player.getBoostType().get(Place.ARTILLERY).equals(BoostType.NORMAL)){
            mainController.getPlayerController().playCard(player,player,Card.buildCard(CardName.Commanders_Horn),null,Place.ARTILLERY,null,false);
        }
    }
    void foltestSteel(Leader leader, Player player, Card pointedCard, Place place){
        Player otherPlayer = mainController.getPlayerController().getOtherPlayer(player);
        if(mainController.getPointController().calculateCombatPoints(otherPlayer)>=10){
            Card remove = null;
            int score = -1;
            for(Card c : otherPlayer.getCardListPlace(Place.ARTILLERY)){
                if(c.getPoints()>score&&!c.getTypes().contains(EffectType.HERO)) {
                    remove = c;
                    score = c.getPoints();
                }
            }
            if(remove != null){
                otherPlayer.getCardListPlace(Place.ARTILLERY).remove(remove);
            }
        }
    }

    void francescaDaisy(Leader leader, Player player, Card pointedCard, Place place){
//
    }

    int getAgilePoints(Player player,Place place){
        List<Card> cardRow = new ArrayList<>(player.getCardListPlace(place));
        List<Card> agileCards = new ArrayList<>();
        boolean hasOlgierd = false;
        boolean hasOlaf = false;
        for(Card c : player.getCardListPlace(place.equals(Place.MELEE)?Place.RANGED:Place.MELEE)){
            if(c.getTypes().contains(EffectType.AGILE)&&!c.getTypes().contains(EffectType.HERO))
                agileCards.add(Card.buildCard(c.getCardName()));
            if(c.getCardName().equals(CardName.Olgierd_von_Everec))
                hasOlgierd = true;
            if(c.getCardName().equals(CardName.Olaf))
                hasOlaf = true;
        }
        if (player.getRowEffects().get(place).contains(EffectType.WEATHER)) {
            for (Card c : agileCards) {
                if(c.getDefaultPoints()!=0)
                    c.setPoints(1);
                else
                    c.setPoints(c.getDefaultPoints()/2);
            }
        }
        for (EffectType ef : player.getRowEffects().get(place)) {
            if (ef.equals(EffectType.MORALEBOOST)) {
                for (Card c : agileCards) {
                    c.setPoints(c.getPoints() + 1);
                }
            }
        }
        if (player.getBoostType().get(place).equals(BoostType.NORMAL)) {
            for (Card c : agileCards) {
                c.setPoints(c.getPoints() * 2);
            }
        }
        cardRow.addAll(agileCards);
        int score = 0;
        for(Card c : cardRow){
            score += c.getPoints();
        }
        if(hasOlgierd) score += cardRow.size()-1;
        if(hasOlaf) score += cardRow.size()-1;
        return score;
    }

    void francescaHope(Leader leader, Player player, Card pointedCard, Place place){
        if(getAgilePoints(player,Place.MELEE)>getAgilePoints(player,Place.RANGED)){
            for(Card c : player.getCardListPlace(Place.RANGED)){
                if(c.getTypes().contains(EffectType.AGILE)&&!c.getTypes().contains(EffectType.HERO))
                    player.getCardListPlace(Place.MELEE).add(c);
            }
            player.getCardListPlace(Place.RANGED).removeIf(c -> c.getTypes().contains(EffectType.AGILE)&&!c.getTypes().contains(EffectType.HERO));
        }else if (getAgilePoints(player,Place.MELEE)<getAgilePoints(player,Place.RANGED)){
            for(Card c : player.getCardListPlace(Place.MELEE)){
                if(c.getTypes().contains(EffectType.AGILE)&&!c.getTypes().contains(EffectType.HERO))
                    player.getCardListPlace(Place.RANGED).add(c);
            }
            player.getCardListPlace(Place.MELEE).removeIf(c -> c.getTypes().contains(EffectType.AGILE)&&!c.getTypes().contains(EffectType.HERO));
        }
    }

    void francescaElf(Leader leader, Player player, Card pointedCard, Place place){
        for(Card c : player.getDrawPile()){
            if(c.getCardName().equals(CardName.Biting_Frost)){
                mainController.getPlayerController().playCard(player,player,c,null,Place.MELEE,player.getDrawPile(),false);
                return;
            }
        }
    }

    void francescaQueen(Leader leader, Player player, Card pointedCard, Place place){
        Player otherPlayer = mainController.getPlayerController().getOtherPlayer(player);
        if(mainController.getPointController().calculateCombatPoints(otherPlayer)>=10){
            Card remove = null;
            int score = -1;
            for(Card c : otherPlayer.getCardListPlace(Place.MELEE)){
                if(c.getPoints()>score&&!c.getTypes().contains(EffectType.HERO)) {
                    remove = c;
                    score = c.getPoints();
                }
            }
            if(remove != null){
                otherPlayer.getCardListPlace(Place.MELEE).remove(remove);
            }
        }
    }

    void francescaBeautiful(Leader leader, Player player, Card pointedCard, Place place){
        if(!player.getBoostType().get(Place.RANGED).equals(BoostType.NORMAL)){
            mainController.getPlayerController().playCard(player,player,Card.buildCard(CardName.Commanders_Horn),null,Place.RANGED,null,false);
        }
    }

    void crach(Leader leader, Player player, Card pointedCard, Place place){
        mainController.getGwent().getGame().getPlayer1().getDrawPile().addAll(player.getDiscardPile());
        Collections.shuffle(mainController.getGwent().getGame().getPlayer1().getDrawPile());
        mainController.getGwent().getGame().getPlayer1().getDiscardPile().clear();
        mainController.getGwent().getGame().getPlayer2().getDrawPile().addAll(player.getDiscardPile());
        Collections.shuffle(mainController.getGwent().getGame().getPlayer2().getDrawPile());
        mainController.getGwent().getGame().getPlayer2().getDiscardPile().clear();
    }

    void kingBran(Leader leader, Player player, Card pointedCard, Place place){
//
    }
}
