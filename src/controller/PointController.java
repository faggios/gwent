package controller;

import model.*;

import java.util.ArrayList;
import java.util.List;

public class PointController {

    private MainController mainController;

    public PointController(MainController mainController) {
        this.mainController = mainController;
    }

    /**
     * calculates all the combat points the given player has
     *
     * @param player the player whose total points shall be calculated
     * @return the points the player has
     */
    public int calculateCombatPoints(Player player) {
        int score = 0;
        for (Place place : player.getCombatPoints().keySet()) {
            score += calculateCombatPoints(player, place);
        }
        return score;
    }

    /**
     * calculates the combat points the given player has for a specific place
     *
     * @param player the player whose points shall be calculated
     * @param place  the place whose points shall be calculated
     * @return the points the player has on that specific place
     */
    public int calculateCombatPoints(Player player, Place place) {
        int score = 0;
        for (Card card : player.getCardListPlace(place)) {
            score += card.getPoints();
        }
        player.getCombatPoints().replace(place, score);
        return score;
    }

    public void refreshCombatPoints(Player player, Place place) {
        List<Card> bondCards = new ArrayList<>();
        for (Card c : player.getCardListPlace(place)) {
            c.setPoints(c.getDefaultPoints());
            if (c.getTypes().contains(EffectType.TIGHTBOND))
                bondCards.add(c);
        }
        boolean weatherHalf = player.getLeader().getName().equals(LeaderName.King_Bran);
        boolean hasWeather = player.getRowEffects().get(place).contains(EffectType.WEATHER);
        if (hasWeather) {
            for (Card c : player.getCardListPlace(place)) {
                if(!weatherHalf&&c.getDefaultPoints()!=0)
                    c.setPoints(1);
                else
                    c.setPoints(c.getDefaultPoints()/2);
            }
        }
        int count1 = 0;
        int count2 = 0;
        for (Card bondCard1 : bondCards) {
            for (Card bondCard2 : bondCards) {
                if (count1 != count2 && bondCard1.getCardName().equals(bondCard2.getCardName())) {
                    bondCard1.setPoints(bondCard1.getPoints() + (hasWeather ? 1 : 8));
                }
                count2++;
            }
            count1++;
        }
        for (EffectType ef : player.getRowEffects().get(place)) {
            if (ef.equals(EffectType.MORALEBOOST)) {
                for (Card c : player.getCardListPlace(place)) {
                    c.setPoints(c.getPoints() + 1);
                }
            }
        }
        if (player.getBoostType().get(place).equals(BoostType.CARD)) {
            for (Card c : player.getCardListPlace(place)) {
                if (!c.getCardName().equals(CardName.Dandelion))
                    c.setPoints(c.getPoints() * 2);
            }
        } else if (player.getBoostType().get(place).equals(BoostType.NORMAL)) {
            for (Card c : player.getCardListPlace(place)) {
                c.setPoints(c.getPoints() * 2);
            }
        }
    }

    public void refreshCombatPoints(Player player, Place place, Card card) {
            card.setPoints(card.getDefaultPoints());
            boolean weatherHalf = player.getLeader().getName().equals(LeaderName.King_Bran);
            if (player.getRowEffects().get(place).contains(EffectType.WEATHER)) {
                if(!weatherHalf&&card.getDefaultPoints()!=0)
                    card.setPoints(1);
                else
                    card.setPoints(card.getDefaultPoints()/2);
            }

            for (EffectType ef : player.getRowEffects().get(place)) {
                if (ef.equals(EffectType.MORALEBOOST))
                    card.setPoints(card.getPoints() + 1);
            }

            if (player.getBoostType().get(place).equals(BoostType.CARD)) {
                if (!card.getCardName().equals(CardName.Dandelion))
                    card.setPoints(card.getPoints() * 2);
            } else if (player.getBoostType().get(place).equals(BoostType.NORMAL)) {
                card.setPoints(card.getPoints() * 2);
            }

    }
}
