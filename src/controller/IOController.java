package controller;

import model.*;

import java.io.*;
import java.util.EnumMap;
import java.util.List;
import java.util.UUID;

public class IOController {

    private MainController mainController;


    private static final String savedir = "src/save";

    private static final String DECKSAVE_SUFIX = ".deck";

    public static  final String CARDDATA = "src/data/gwent_data.csv";

    public static  final String LEADERDATA = "src/data/leader_data.csv";

    public IOController(MainController mainController)
    {
        this.mainController=mainController;
    }

    public void save(){
        List<Deck> decks = mainController.getGwent().getDecks();
        for(Deck deck : decks){
            File file = new File(savedir+"/"+deck.getUniqueID()+DECKSAVE_SUFIX);
            try {

                ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(file));
                stream.writeObject(deck);
                stream.close();
            }  catch (IOException e) {
                System.err.println("Speicherfehler in der Main");
                e.printStackTrace();
            }
        }
    }

    public void deleteDeckFile(UUID uuid){
        //TODO: die Datei mit der übergebenen UUID als name löschen
        File toDelete = new File(savedir+"/"+uuid+DECKSAVE_SUFIX);
        toDelete.delete();
    }

    public Gwent load(){
        Gwent main=new Gwent();
        File[] files = new File(savedir).listFiles();
        if(files != null) {
            for (File file : files) {
                if (!file.getAbsolutePath().endsWith(".deck"))
                    continue;
                try {
                    ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file));

                    Deck deck = (Deck) stream.readObject();
                    main.getDecks().add(deck);
                    stream.close();
                } catch (Exception e) {
                    System.err.println("Ladefehler bei der Main");
                    e.printStackTrace();
                }
            }
        }
        EnumMap<FactionName,Faction> factions = new EnumMap<>(FactionName.class);
        for(FactionName factionName : FactionName.values()){
            factions.put(factionName,new Faction(factionName));
        }

        //add cards
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(CARDDATA));
            bufferedReader.readLine();
            String zeile;
            while ((zeile = bufferedReader.readLine()) != null) {
                String[] data = zeile.split(";");
                FactionName faction = FactionName.valueOf(data[4]);
                CardName name = CardName.valueOf(data[0]);
                factions.get(faction).addCard(name);
            }
        } catch (FileNotFoundException e) {
            System.out.println("file " +CARDDATA + " not found");
        } catch (IOException e) {
            System.out.println("Lesefehler");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println("fehlerhafter Dateiinhalt1");
        }

        //leader
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(LEADERDATA));
            bufferedReader.readLine();
            String zeile;
            while ((zeile = bufferedReader.readLine()) != null) {
                String[] data = zeile.split(";");
                FactionName faction = FactionName.valueOf(data[1]);
                LeaderName name = LeaderName.valueOf(data[0]);
                factions.get(faction).addLeader(name);
            }
        } catch (FileNotFoundException e) {
            System.out.println("file " +CARDDATA + " not found");
        } catch (IOException e) {
            System.out.println("Lesefehler");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println("fehlerhafter Dateiinhalt2");
        }
        for(FactionName factionName : FactionName.values()){
            main.getFactions().add(factions.get(factionName));
        }
        return main;
    }
}
