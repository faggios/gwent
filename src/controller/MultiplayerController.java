package controller;

import Net.GameClient;
import Net.GameServer;
import Net.NetService;
import model.Game;
import model.Player;
import view.lobby.LobbyController;
import java.io.*;


public class MultiplayerController {

    private MainController mainController;

    public static int PORTNUM=7777;

    public static boolean isHost=false;
    private NetService netService = null;
    public MultiplayerController(MainController mainController){
        this.mainController = mainController;
    }

    public String getIP(){
        return netService!=null?netService.getClient().getInetAddress().toString():"";
    }

    public void startHost(LobbyController lobby){
        if(netService==null){
            try {
                netService=new GameServer(lobby,mainController);
                isHost=true;
                System.out.println("Host started");
            } catch (IOException e) {
                if(netService!=null)
                    netService.stop();
                netService=null;
                e.printStackTrace();
                System.out.println("Host starting failed");
            }
        }
    }
    public void startClient(LobbyController lobby,String hostip){
        if(netService==null){
            try {
                netService=new GameClient(lobby,mainController,hostip);
                isHost=false;
                System.out.println("Client started");
            } catch (IOException e) {
                if(netService!=null)
                    netService.stop();
                netService=null;
                e.printStackTrace();
                System.out.println("Client starting failed");
            }
        }
    }
    public void stop(){
        if(netService!=null)
            netService.stop();
        netService = null;
        System.out.println("Stopped NetServices");
    }
    public Game recieveGame(){
        Object result = recieve();
        if(result instanceof Game)
            return (Game) result;
        else {
            System.out.println("Receiving game failed");
            return null;
        }
    }
    public int recieveInt(){
        Object result = recieve();
        if(result instanceof Integer)
            return (int) result;
        else {
            System.out.println("Receiving int failed");
            return 0;
        }
    }
    public String recieveString(){
        Object result = recieve();
        if(result instanceof String)
            return (String) result;
        else {
            System.out.println("Receiving String failed");
            return null;
        }
    }
    public Player recievePlayer(){
        Object result = recieve();
        if(result instanceof Player)
            return (Player) result;
        else {
            System.out.println("Receiving Player failed");
            return null;
        }
    }
    public void send(Object object){
        if(netService==null) {
            System.out.println("Send failed: no netService");
            return;
        }
        netService.sendObject(object);
    }

    public Object recieve(){
        if(netService==null) {
            System.out.println("receive failed: no netService");
            return null;
        }
        return netService.receiveObject();
    }
}
