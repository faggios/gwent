package controller;

import model.*;

import java.util.*;

import model.CardName;
import view.selectCardView.SelectCardController;

public class EffectController {

    private MainController mainController;

    public EffectController(MainController mainController) {
        this.mainController = mainController;
    }

    /**
     * If a parameter doesnt make sense just set it to null
     *
     * @param card        card that has been played
     * @param player      player that is affected by the effects of the card (the player that played the card if scorch, player that gets the spy card played to)
     * @param pointedCard the card which is being pointed at
     * @param place       the row, where card has been placed at / applied to
     */
    public void doEffect(Card card, Player player, Card pointedCard, Place place) {
        boolean cardPointsRefresh = false;
        boolean allPointsRefresh = false;
        for (EffectType effectType : card.getTypes()) {
            switch (effectType) {
                case COMBAT:
                    combatEffect(card, player, place);
                    cardPointsRefresh = true;
                    break;
                case AGILE:
                    agileEffect(card, player);
                    break;
                case HERO:
                    heroEffect(card, player);
                    cardPointsRefresh = false;
                    break;
                case MEDIC:
                    medicEffect(card, player);
                    break;
                case MORALEBOOST:
                    moraleboostEffect(card, player, place);
                    allPointsRefresh = true;
                    break;
                case MUSTER:
                    musterEffect(card, player, place);
                    break;
                case SPY:
                    spyEffect(card, player);
                    break;
                case TIGHTBOND:
                    tightbondEffect(card, player, place);
                    allPointsRefresh = true;
                    break;
                case WEATHER:
                    weatherEffect(card, player);
                    allPointsRefresh = true;
                    break;
                case CLEARWEATHER:
                    clearweatherEffect(card, player);
                    allPointsRefresh = true;
                    break;
                case DECOY:
                    decoyEffect(card, player, pointedCard);
                    allPointsRefresh = true;
                    break;
                case HORNBOOST:
                    hornboostEffect(card, player, place);
                    allPointsRefresh = true;
                    break;
                case MARDROEME:
                    madroemeEffect(card, player, place);
                    break;
                case SCORCH:
                    scorchEffect(card, player, pointedCard, place);
                    allPointsRefresh = true;
                    break;
                case BERSERKER:
                    berserkerEffect(card, player, place);
                    break;
                case SUMMONAVANGER:
                    summonAvangerEffect(card, player, place);
                    break;
            }
        }
        if (allPointsRefresh) {
            mainController.getPointController().refreshCombatPoints(player, place);
        } else if (cardPointsRefresh) {
            mainController.getPointController().refreshCombatPoints(player, place, card);
        }
    }

    public void combatEffect(Card card, Player player, Place place) {
//
    }

    void agileEffect(Card card, Player player) {
//
    }

    void heroEffect(Card card, Player player) {
//
    }

    void medicEffect(Card card, Player player) {
//view anzeigen und darin karte auswählen und spielen
        mainController.getStackPane().getChildren().add(new SelectCardController(mainController.getStackPane(),mainController,3,player,player.getDiscardPile(),false,null));
    }

    void moraleboostEffect(Card card, Player player, Place place) {
        player.getRowEffects().get(place).add(EffectType.MORALEBOOST);
    }

    void musterEffect(Card card, Player player, Place place) {
        if(card.getCardName().toString().startsWith("Crone")){
            playCardsThatStartWith(player,"Crone",null,place);
        }
        else if(card.getCardName().equals(CardName.Gaunter_ODimm_Darkness)){
            playCardsThatStartWith(player,null,CardName.Gaunter_ODimm_Darkness,place);
        }
        else if(card.getCardName().equals(CardName.Gaunter_ODimm)){
            playCardsThatStartWith(player,"Gaun",null,place);
        }
        else if(card.getCardName().equals(CardName.Nekker)){
            playCardsThatStartWith(player,null,CardName.Nekker,place);
        }
        else if(card.getCardName().toString().startsWith("Vam")) {
            playCardsThatStartWith(player, "Vam", null, place);
        }
        else if(card.getCardName().equals(CardName.Ghoul)){
            playCardsThatStartWith(player,null,CardName.Ghoul,place);
        }
        else if(card.getCardName().equals(CardName.Dwarven_Skirmisher)){
            playCardsThatStartWith(player,null,CardName.Dwarven_Skirmisher,place);
        }
        else if(card.getCardName().equals(CardName.Havekar_Smuggler)){
            playCardsThatStartWith(player,null,CardName.Havekar_Smuggler,place);
        }
        else if(card.getCardName().equals(CardName.Light_Longship)){
            playCardsThatStartWith(player,null,CardName.Light_Longship,place);
        }
        else if(card.getCardName().equals(CardName.Elven_Skirmisher)){
            playCardsThatStartWith(player,null,CardName.Elven_Skirmisher,place);
        }
        else if(card.getCardName().equals(CardName.Arachas_Behemoth)){
            playCardsThatStartWith(player,"Arachas",null,place);
        }
        else if(card.getCardName().equals(CardName.Arachas)){
            playCardsThatStartWith(player,null,CardName.Arachas,place);
        }
        else if(card.getCardName().equals(CardName.Cerys)){
            playCardsThatStartWith(player,null,CardName.Clan_Drummond_Shield_Maiden,place);
        }
        else{
            System.out.println("This card doesnt summon anything, this effect shouldnt have been triggered");
        }
    }

    private void playCardsThatStartWith(Player player,String start,CardName cardName, Place place){
        if(start!=null){
            for(Card c : player.getHandcards()){
                if(c.getCardName().toString().startsWith(start))
                    mainController.getPlayerController().playCard(player,player,c,null,place,player.getHandcards(),true);
            }
            for(Card c : player.getDrawPile()){
                if(c.getCardName().toString().startsWith(start))
                    mainController.getPlayerController().playCard(player,player,c,null,place,player.getDrawPile(),true);
            }
        }else{
            for(Card c : player.getHandcards()){
                if(c.getCardName().equals(cardName))
                    mainController.getPlayerController().playCard(player,player,c,null,place,player.getHandcards(),true);
            }
            for(Card c : player.getDrawPile()){
                if(c.getCardName().equals(cardName))
                    mainController.getPlayerController().playCard(player,player,c,null,place,player.getDrawPile(),true);
            }
        }

    }

    void spyEffect(Card card, Player player) {
        mainController.getPlayerController().drawNewCard(player, 2);
    }

    void tightbondEffect(Card card, Player player, Place place) {
//
    }

    void weatherEffect(Card card, Player player) {
        for (Place place : Place.values()) {
            if (!mainController.getGwent().getGame().getPlayer1().getRowEffects().get(place).contains(EffectType.WEATHER)) {
                mainController.getGwent().getGame().getPlayer1().getRowEffects().get(place).add(EffectType.WEATHER);
                mainController.getGwent().getGame().getPlayer2().getRowEffects().get(place).add(EffectType.WEATHER);
            }
        }
    }

    void clearweatherEffect(Card card, Player player) {
        for (Place place : Place.values()) {
            mainController.getGwent().getGame().getPlayer1().getRowEffects().get(place).remove(EffectType.WEATHER);
            mainController.getGwent().getGame().getPlayer2().getRowEffects().get(place).remove(EffectType.WEATHER);
        }
    }

    void decoyEffect(Card card, Player player, Card pointedCard) {
        if (player.getCardListPlace(Place.MELEE).contains(pointedCard)) {
            player.getHandcards().add(pointedCard);
            player.getHandcards().remove(card);
            player.getCardListPlace(Place.MELEE).remove(pointedCard);
            player.getCardListPlace(Place.MELEE).add(card);
        } else if (player.getCardListPlace(Place.RANGED).contains(pointedCard)) {
            player.getHandcards().add(pointedCard);
            player.getHandcards().remove(card);
            player.getCardListPlace(Place.RANGED).remove(pointedCard);
            player.getCardListPlace(Place.RANGED).add(card);
        } else if (player.getCardListPlace(Place.ARTILLERY).contains(pointedCard)) {
            player.getHandcards().add(pointedCard);
            player.getHandcards().remove(card);
            player.getCardListPlace(Place.ARTILLERY).remove(pointedCard);
            player.getCardListPlace(Place.ARTILLERY).add(card);
        } else {
            System.out.println("You didn't select a valid card to use the decoy effect on");
        }
        pointedCard.setPoints(pointedCard.getDefaultPoints());
    }

    void hornboostEffect(Card card, Player player, Place place) {
        if(!player.getBoostType().get(place).equals(BoostType.NORMAL)) {
            player.getRowEffects().get(place).add(EffectType.HORNBOOST);
            if(card.getTypes().contains(EffectType.COMBAT))
                player.getBoostType().replace(place,BoostType.CARD);
            else
                player.getBoostType().replace(place,BoostType.NORMAL);
        }else{
            player.getRowEffects().get(place).add(EffectType.HORNBOOST);
        }
    }

    void madroemeEffect(Card card, Player player, Place place) {
        if (!player.getRowEffects().get(place).contains(EffectType.MARDROEME))
            player.getRowEffects().get(place).add(EffectType.MARDROEME);
        for (int index = 0; index < player.getCardList().get(place).size(); index++) {
            if (player.getCardList().get(place).get(index).getTypes().contains(EffectType.BERSERKER)) {
                Card bear = Card.buildCard(player.getCardList().get(place).get(index).getCardName().equals(CardName.Young_Berserker) ? CardName.Transformed_Young_Vildkaarl : CardName.Transformed_Vildkaarl);
                mainController.getPlayerController().playCard(player, player, bear, null, place, null, false);
                player.getCardList().get(place).remove(index);
            }
        }
        player.getRowEffects().get(place).removeIf(effect -> effect.equals(EffectType.BERSERKER));
    }

    void scorchEffect(Card card, Player player, Card pointedCard, Place place) {//TODO effekte entfernen (moralboost), cow-> effekt triggern?
        Player player2 = mainController.getGwent().getGame().getPlayer1().equals(player) ? player : mainController.getGwent().getGame().getPlayer2();
        switch (card.getCardName()) {
            case Villentretenmerth:
                if (player2.getCombatPoints().get(Place.MELEE) >= 10) {
                    List<Card> toRemove = getBiggestCards(player2, Place.MELEE);
                    if (toRemove != null && !toRemove.isEmpty())
                        player2.getCardList().get(Place.MELEE).removeAll(toRemove);
                }
                break;
            case Clan_Dimun_Pirate:
                removeBiggestCard();
                break;
            case Toad:
                if (player2.getCombatPoints().get(Place.RANGED) >= 10) {
                    List<Card> toRemove = getBiggestCards(player2, Place.RANGED);
                    if (toRemove != null && !toRemove.isEmpty())
                        player2.getCardList().get(Place.RANGED).removeAll(toRemove);
                }
                break;
            case Schirru:
                if (player2.getCombatPoints().get(Place.ARTILLERY) >= 10) {
                    List<Card> toRemove = getBiggestCards(player2, Place.ARTILLERY);
                    if (toRemove != null && !toRemove.isEmpty())
                        player2.getCardList().get(Place.ARTILLERY).removeAll(toRemove);
                }
                break;
            case Scorch:
                removeBiggestCard();
                break;
        }
    }

    void berserkerEffect(Card card, Player player, Place place) {
        if (!player.getRowEffects().get(place).contains(EffectType.MARDROEME)) {
            player.getRowEffects().get(place).add(EffectType.BERSERKER);
        } else {
            player.getCardListPlace(place).remove(card);
            Card bear = Card.buildCard(card.getCardName().equals(CardName.Young_Berserker) ? CardName.Transformed_Young_Vildkaarl : CardName.Transformed_Vildkaarl);
            mainController.getPlayerController().playCard(player, player, bear, null, place, null, false);
        }
    }

    void summonAvangerEffect(Card card, Player player, Place place) {
        player.getRowEffects().get(place).add(EffectType.SUMMONAVANGER);
    }

    private List<Card> getBiggestCards(Player player, Place place) {
        List<Card> biggestCard = null;
        if (place != null) {
            int biggestScore = 0;
            for (Card temp : player.getCardList().get(place)) {
                if (!temp.getTypes().contains(EffectType.HERO)) continue;
                if (biggestCard == null) {
                    biggestCard = new ArrayList<>();
                    biggestCard.add(temp);
                    biggestScore = temp.getPoints();
                } else if (biggestScore == temp.getPoints()) {
                    biggestCard.add(temp);
                } else if (biggestScore < temp.getPoints()) {
                    biggestCard.clear();
                    biggestCard.add(temp);
                    biggestScore = temp.getPoints();
                }
            }
        }
        return biggestCard;
    }

    private void removeBiggestCard() {
        int biggestScore = -1;
        for (Place place : Place.values()) {
            for (Card temp : mainController.getGwent().getGame().getPlayer1().getCardList().get(place)) {
                if (temp.getPoints() > biggestScore && !temp.getTypes().contains(EffectType.HERO)) {
                    biggestScore = temp.getPoints();
                }
            }
        }
        for (Place place : Place.values()) {
            for (Card temp : mainController.getGwent().getGame().getPlayer2().getCardList().get(place)) {
                if (temp.getPoints() > biggestScore && !temp.getTypes().contains(EffectType.HERO)) {
                    biggestScore = temp.getPoints();
                }
            }
        }
        if (biggestScore > -1) {
            final int finalBiggestScore = biggestScore;
            for (Place place : Place.values()) {
                mainController.getGwent().getGame().getPlayer1().getCardList().get(place).removeIf(temp -> temp.getPoints() == finalBiggestScore && !temp.getTypes().contains(EffectType.HERO));
            }
            for (Place place : Place.values()) {
                mainController.getGwent().getGame().getPlayer2().getCardList().get(place).removeIf(temp -> temp.getPoints() == finalBiggestScore && !temp.getTypes().contains(EffectType.HERO));
            }
        }
    }
}
