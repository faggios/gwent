package controller;

import model.*;

import java.util.List;

public class PlayerController {

    private MainController mainController;

    public PlayerController(MainController mainController)
    {
        this.mainController=mainController;
    }

    public boolean isDead(Player player){
        return player.getLifePoints()==0;
    }

    public void reduceLifePoints(Player player){
        if(isDead(player))
            return;
        player.setLifePoints(player.getLifePoints()-1);

    }
    public int sumForce(Player player){
        int sum = 0;
        for(Place place : Place.values())
            sum+=player.getCombatPoints().get(place);
        return sum;
    }

    /** plays the card
     *
     * @param player the player that build the card
     * @param buildTo the player where the cards gets placed at
     * @param card the card that gets played
     * @param pointedCard the card that the effect points on (can be null)
     * @param place the place that the card gets placed at (can be null)
     * @param fromWhichList the list where the card was at and needs to be removed from (can be null)
     * @param noEffect sets if the card shall be played without applying it's effect
     */
    public void playCard(Player player, Player buildTo, Card card, Card pointedCard, Place place, List<Card> fromWhichList, Boolean noEffect) {
        if(place!=null)
            buildTo.getCardList().get(place).add(card);
        if(noEffect!=null&&!noEffect)
            mainController.getEffectController().doEffect(card, buildTo, pointedCard, place);
        if(fromWhichList!=null)
            fromWhichList.remove(card);
    }

    public void playLeader(Player player, Leader leader, Card pointedCard, Place place, boolean noEffect) {
        if(mainController.getGwent().getGame().getPlayer1().getLeader().getName().equals(LeaderName.Emhyr_var_Emreis_The_White_Flame)
                ||mainController.getGwent().getGame().getPlayer2().getLeader().getName().equals(LeaderName.Emhyr_var_Emreis_The_White_Flame))
            return;
        leader.setUsed(true);
        if(!noEffect)
            mainController.getLeaderEffectController().doEffect(leader, player, pointedCard, place);
    }

    public void drawNewCard(Player player,int amount){
        while(!player.getDrawPile().isEmpty() & amount>0) {
            player.getHandcards().add(player.getDrawPile().remove(0));
            amount--;
        }
    }

    public Player getOtherPlayer(Player player){
        return player.equals(mainController.getGwent().getGame().getPlayer1())?mainController.getGwent().getGame().getPlayer2():mainController.getGwent().getGame().getPlayer1();
    }
}
