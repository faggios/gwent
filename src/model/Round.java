package model;

import java.io.Serializable;

public enum Round implements Serializable {
    SETUP,
    ONE,
    TWO,
    THREE,
    END
}
