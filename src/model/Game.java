package model;


import org.javatuples.Pair;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Game implements Serializable {

    private Player player1;
    private Player player2;
    private int currentPlayer;// 0 -> fehler, 1 -> player1, 2 -> playyer2
    private Round round;
    private Map<Round,Integer> winnerMap; //0
    private Map<Round, Pair<Integer,Integer>>scoreMap;

    public Game(){
        player1 = null;
        player2 = null;
        currentPlayer = 0;
        round = Round.SETUP;
        scoreMap = new HashMap<>();
        winnerMap = new HashMap<>();
    }

    public Map<Round, Integer> getWinnerMap() { return winnerMap; }

    public void setWinnerMap(Map<Round, Integer> winnerMap) { this.winnerMap = winnerMap; }

    public Map<Round, Pair<Integer, Integer>> getScoreMap() { return scoreMap; }

    public void setScoreMap(Map<Round, Pair<Integer, Integer>> scoreMap) { this.scoreMap = scoreMap; }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
}
