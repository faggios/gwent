package model;

import java.io.Serializable;

public enum Place implements Serializable {
    MELEE,
    RANGED,
    ARTILLERY
}
