package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Gwent implements Serializable {

    private List<Deck> decks;
    private Game game;
    private List<Card> cards;
    private List<Leader> leaders;
    private List<Faction> factions;

    public Gwent(){
        decks = new LinkedList<>();
        game = null;
        cards = new LinkedList<>();
        leaders = new LinkedList<>();
        factions = new LinkedList<>();
    }

    public List<Deck> getDecks() {
        return decks;
    }

    public void setDecks(LinkedList<Deck> decks) {
        this.decks = decks;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public List<Leader> getLeaders() {
        return leaders;
    }

    public void setLeaders(List<Leader> leaders) {
        this.leaders = leaders;
    }

    public List<Faction> getFactions() {
        return factions;
    }

    public Faction getFaction(FactionName factionName) {
        for(Faction f : factions) {
            if (f.getName().equals(factionName))
                return f;
        }
        return null;
    }

    public void setFactions(List<Faction> factions) {
        this.factions = factions;
    }
}
