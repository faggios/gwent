package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class Deck implements Serializable {

    private Faction faction;
    private Leader leader;
    private List<Card> cards;
    private String deckName;
    private boolean isUsable;
    private int totalPoints;
    private int totalHeros;
    private UUID uniqueID;

    public Deck(){
        faction = null;
        leader = null;
        cards = new LinkedList<>();
        deckName = "";
        isUsable = false;
        totalPoints = 0;
        totalHeros = 0;
        uniqueID = UUID.randomUUID();
    }

    public UUID getUniqueID() { return uniqueID; }

    public int getTotalHeros() { return totalHeros; }

    public void setTotalHeros(int totalHeros) { this.totalHeros = totalHeros; }

    public int getTotalPoints() { return totalPoints; }

    public void setTotalPoints(int totalPoints) { this.totalPoints = totalPoints; }

    public boolean isUsable() { return isUsable; }

    public void setUsable(boolean usable) { isUsable = usable; }

    public String getName(){ return deckName; }

    public void setDeckName(String name){deckName = name;}

    public Faction getFaction() { return faction; }

    public void setFaction(Faction faction) { this.faction = faction; }

    public Leader getLeader() { return leader; }

    public void setLeader(Leader leader) { this.leader = leader; }

    public List<Card> getCards() { return cards; }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

}
