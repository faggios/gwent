package model;

import java.io.Serializable;

public enum FactionName implements Serializable {
    MONSTER,
    NILFGAARDIAN_EMPIRE,
    NORTHERN_REALMS,
    SCOIATAEL,
    SKELLIGE,
    NEUTRAL,
}
