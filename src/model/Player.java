package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;

public class Player implements Serializable {

    private Faction faction;
    private Leader leader;
    private EnumMap<Place, List<Card>> cardList;
    private List<Card> handcards;
    private List<Card> drawPile;
    private List<Card> discardPile;
    private int lifePoints = 2;
    private EnumMap<Place, Integer> combatPoints;
    private String playerName;
    private Deck deck;

    private EnumMap<Place, BoostType> boostType;

    private EnumMap<Place,List<EffectType>> rowEffects;

    public Player(String playerName, Faction faction, Leader leader,Deck deck) {
        cardList = new EnumMap<>(Place.class);
        for (Place place : Place.values())
            cardList.put(place, new LinkedList<>());

        combatPoints = new EnumMap<>(Place.class);
        for (Place place : Place.values())
            combatPoints.put(place, 0);

        handcards = new LinkedList<>();
        drawPile = new LinkedList<>();
        this.playerName = playerName;
        this.faction = faction;
        this.deck = deck;
        this.leader = leader;
        discardPile = new LinkedList<>();

        for(Card c : deck.getCards()){
            drawPile.add(Card.buildCard(c.getCardName()));
        }

        boostType = new EnumMap<>(Place.class);
        boostType.put(Place.MELEE, BoostType.NONE);
        boostType.put(Place.RANGED, BoostType.NONE);
        boostType.put(Place.ARTILLERY, BoostType.NONE);

        rowEffects = new EnumMap<>(Place.class);
        for (Place place : Place.values())
            rowEffects.put(place, new ArrayList<>());
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) { this.deck = deck; }

    public EnumMap<Place, BoostType> getBoostType() {
        return boostType;
    }

    public EnumMap<Place, List<EffectType>> getRowEffects() { return rowEffects; }

    public void setBoostType(EnumMap<Place, BoostType> boostType) { this.boostType = boostType; }

    public Faction getFaction() {
        return faction;
    }

    public void setFaction(Faction faction) {
        this.faction = faction;
    }

    public Leader getLeader() {
        return leader;
    }

    public void setLeader(Leader leader) {
        this.leader = leader;
    }

    public EnumMap<Place, List<Card>> getCardList() {
        return cardList;
    }

    public void setCardList(EnumMap<Place, List<Card>> cardList) {
        this.cardList = cardList;
    }

    public List<Card> getCardListPlace(Place place) {
        return cardList.get(place);
    }

    public void setCardList(Place place, List<Card> cards) {
        this.cardList.replace(place, cards);
    }

    public List<Card> getHandcards() {
        return handcards;
    }

    public void setHandcards(List<Card> handcards) {
        this.handcards = handcards;
    }

    public List<Card> getDrawPile() {
        return drawPile;
    }

    public void setDrawPile(List<Card> drawPile) {
        this.drawPile = drawPile;
    }

    public List<Card> getDiscardPile() {
        return discardPile;
    }

    public void setDiscardPile(List<Card> discardPile) {
        this.discardPile = discardPile;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public EnumMap<Place, Integer> getCombatPoints() {
        return combatPoints;
    }

    public void setCombatPoints(EnumMap<Place, Integer> combatPoints) {
        this.combatPoints = combatPoints;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
