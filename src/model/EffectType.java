package model;

import java.io.Serializable;

public enum EffectType implements Serializable{
    COMBAT,
    AGILE,
    HERO,
    MEDIC,
    MORALEBOOST,
    MUSTER,
    SPY,
    TIGHTBOND,
    WEATHER,
    CLEARWEATHER,
    DECOY,
    HORNBOOST,
    MARDROEME,
    SCORCH,
    BERSERKER,
    SUMMONAVANGER
}
