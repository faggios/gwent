package model;

import controller.IOController;

import java.io.*;

public class Leader implements Serializable {

    private boolean used;
    public final LeaderName name;

    public Leader(LeaderName name){
        this.name = name;
        used = false;
    }
    public LeaderName getName() { return name; }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public static Leader buildLeader(LeaderName name){//TODO ergänzen
        Leader result = null;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(IOController.LEADERDATA));
            String zeile = bufferedReader.readLine();
            assert zeile.equals("age,card");
            while ((zeile = bufferedReader.readLine()) != null && !zeile.startsWith(name.name())) {}
            String[] data = zeile.split(";");

            result= new Leader(name);

        } catch (FileNotFoundException e) {
            System.out.println("file " + IOController.LEADERDATA.toString() + " not found");
        } catch (IOException e) {
            System.out.println("Lesefehler");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println("fehlerhafter Dateiinhalt");
        }
        return result;
    }
}
