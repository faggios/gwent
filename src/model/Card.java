package model;

import controller.IOController;

import java.io.*;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;

public class Card implements Serializable {

    private EnumMap<Place,Boolean> places;
    private List<EffectType> types;
    private int points;
    private final int defaultPoints;

    private CardName cardName;

    public Card(int defaultPoints){
        this.defaultPoints = defaultPoints;
        places = new EnumMap<>(Place.class);
        places.put(Place.MELEE,false);
        places.put(Place.RANGED,false);
        places.put(Place.ARTILLERY,false);
        types = new LinkedList<>();

        points = 0;
    }

    public int getDefaultPoints() {
        return defaultPoints;
    }

    public CardName getCardName() {
        return cardName;
    }

    public void setCardName(CardName cardName) {
        this.cardName = cardName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) { this.points = points; }

    public EnumMap<Place, Boolean> getPlaces() {
        return places;
    }

    public void setPlaces(EnumMap<Place, Boolean> places){ this.places = places; }

    public List<EffectType> getTypes() {
        return types;
    }

    public void addPlace(Place place){places.replace(place,true); }

    public void removePlace(Place place){places.put(place,false);}

    public boolean getPlace(Place place){ return places.get(place); }

    public void addType(EffectType type){types.add(type);}

    public boolean hasType(EffectType type){
        for(EffectType htype : types){
            if(type==htype)
                return true;
        }
        return false;
    }

    public static Card buildCard(CardName name){//TODO ergänzen
        int defaultPoints;
        Card result = null;


        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(IOController.CARDDATA));
            String zeile = bufferedReader.readLine();
            assert zeile.equals("age,card");
            while ((zeile = bufferedReader.readLine()) != null && !zeile.startsWith(name.name())) {}
            String[] data = zeile.split(";");
            if(data[1].equals(""))
                defaultPoints=(0);
            else
                defaultPoints= Integer.parseInt(data[1]);

            result= new Card(defaultPoints);
            result.setCardName(name);

            for(String type : data[2].split(",")){
                result.addType(EffectType.valueOf(type));
            }
            if(!data[3].isEmpty()) {
                for (String place : data[3].split(",")) {
                    result.addPlace(Place.valueOf(place));
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("file " + IOController.CARDDATA + " not found");
        } catch (IOException e) {
            System.out.println("Lesefehler");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println("fehlerhafter Dateiinhalt");
        }
        return result;
    }
}
