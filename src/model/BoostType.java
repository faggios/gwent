package model;

import java.io.Serializable;

public enum BoostType implements Serializable {
    NONE,
    CARD,
    NORMAL
}
