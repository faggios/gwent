package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Faction implements Serializable {

    public final FactionName name;

    List<CardName> cards=new LinkedList<>();
    List<LeaderName> leaders=new LinkedList<>();

    public Faction(FactionName name){

        this.name = name;
    }
    public void addCard(CardName cardName){
        cards.add(cardName);
    }

    public void addLeader(LeaderName leaderName){
        leaders.add(leaderName);
    }

    public FactionName getName() { return name; }

    public List<CardName> getCards(){return cards;}

    public List<LeaderName> getLeaders(){return leaders;}
}
