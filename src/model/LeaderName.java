package model;

import java.io.Serializable;

public enum LeaderName implements Serializable {
    Eredin_Breacc_Glas_The_Treacherous,
    Eredin_Bringer_of_Death,
    Eredin_Commander_of_the_Red_Riders,
    Eredin_Destroyer_of_Worlds,
    Eredin_King_of_the_Wild_Hunt,
    Emhyr_var_Emreis_Emperor_of_Nilfgaard,
    Emhyr_var_Emreis_His_Imperial_Majesty,
    Emhyr_var_Emreis_Invader_of_the_North,
    Emhyr_var_Emreis_The_Relentless,
    Emhyr_var_Emreis_The_White_Flame,
    Foltest_King_of_Temeria,
    Foltest_Lord_Commander_of_the_North,
    Foltest_Son_of_Medell,
    Foltest_The_Siegemaster,
    Foltest_The_SteelForged,
    Francesca_Findabair_Daisy_of_the_Valley,
    Francesca_Findabair_Hope_of_the_Aen_Seidhe,
    Francesca_Findabair_Pureblood_Elf,
    Francesca_Findabair_Queen_of_Dol_Blathanna,
    Francesca_Findabair_The_Beautiful,
    Crach_an_Craite,
    King_Bran
}
