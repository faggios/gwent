package view.pauseMenu;

import controller.MainController;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import view.mainview.MainviewController;
import view.settings.SettingsController;

import java.io.IOException;

public class PauseMenuController extends BorderPane {

    private StackPane stackPane;
    private MainController mainController;

    public PauseMenuController(StackPane stackPane, MainController mainController){
        this.stackPane = stackPane;
        this.mainController = mainController;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/pauseMenu/PauseMenu.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void backToDesktopClicked(ActionEvent event) {
        mainController.getMultiplayerController().stop();
        ((Stage) this.getScene().getWindow()).close();
        mainController.getIoController().save();
    }

    @FXML
    void backToMainmenuClicked(ActionEvent event) { //TODO irgenwas speichern ?
        mainController.getMultiplayerController().stop();
        stackPane.getChildren().clear();
        stackPane.getChildren().add(new MainviewController(stackPane,mainController));
    }

    @FXML
    void backToResumeClicked(ActionEvent event) {
        stackPane.getChildren().remove(this);
    }

    @FXML
    void backToSettingsClicked(ActionEvent event) {
        stackPane.getChildren().add(new SettingsController(stackPane,mainController));
        stackPane.getChildren().remove(this);
    }
}
