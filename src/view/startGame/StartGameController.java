package view.startGame;

import controller.MainController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import view.lobby.LobbyController;
import view.mainview.MainviewController;

import java.io.IOException;

public class StartGameController extends BorderPane {

    @FXML
    private Button backButton;

    @FXML
    private TextField playerNameTextField;

    @FXML
    private TextField ipAddressTextField;

    @FXML
    private TextField portTextField;

    @FXML
    private Label ipAddressLabel;

    @FXML
    private Label portLabel;

    @FXML
    private Button hostGameButton;

    @FXML
    private Button joinGameButton;

    private MainController mainController;
    private StackPane stackPane;

    public StartGameController(MainController mainController, StackPane stackPane){
        this.mainController = mainController;
        this.stackPane = stackPane;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/startGame/StartGame.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize(){
        this.prefHeightProperty().bind(stackPane.heightProperty());
        this.prefWidthProperty().bind(stackPane.widthProperty());

        portTextField.setText(7777+"");
        ipAddressLabel.setText("IP-Address: "+"");
        portLabel.setText("Port: "+7777);
    }

    @FXML
    void backPressed(ActionEvent event) {
        stackPane.getChildren().remove(this);
        stackPane.getChildren().add(new MainviewController(stackPane,mainController));
    }

    @FXML
    void hostGamePressed(ActionEvent event) {
        LobbyController lobby = new LobbyController(mainController,stackPane,true,playerNameTextField.getText());
        mainController.getMultiplayerController().startHost(lobby);
        stackPane.getChildren().remove(this);
        stackPane.getChildren().add(lobby);
    }

    @FXML
    void joinGamePressed(ActionEvent event) {
        LobbyController lobby = new LobbyController(mainController,stackPane,false,playerNameTextField.getText());
        mainController.getMultiplayerController().startClient(lobby,ipAddressTextField.getText());

        System.out.println("Client started, now sending username");
        mainController.getMultiplayerController().send(playerNameTextField.getText());
        System.out.println("Name send, now getting host name");
        lobby.playerJoined(mainController.getMultiplayerController().recieveString());

        stackPane.getChildren().remove(this);
        stackPane.getChildren().add(lobby);
    }

}
