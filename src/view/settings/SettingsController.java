package view.settings;
import controller.MainController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.IOException;

public class SettingsController extends BorderPane {

    @FXML
    private Button backToDesktopButton;

    private StackPane stackPane;
    private MainController mainController;

    public SettingsController(StackPane stackPane, MainController mainController){
        this.stackPane = stackPane;
        this.mainController = mainController;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/settings/Settings.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void backToDesktopClicked(ActionEvent event) {
        ((Stage) backToDesktopButton.getScene().getWindow()).close();
    }

}
