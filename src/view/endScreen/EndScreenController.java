package view.endScreen;

import controller.MainController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import model.Round;
import org.javatuples.Pair;
import view.mainview.MainviewController;

import java.io.IOException;
import java.util.Map;

public class EndScreenController extends BorderPane {

    @FXML
    private ImageView resultImage;
    @FXML
    private Text playerOneText, playerTwoText, rOnePOneScore, rOnePTowScore, rTwoPOneScore, rTwoPTwoScore, rThreePOneScore, rThreePTwpScore;
    @FXML
    private Button backButton;

    private StackPane stackPane;
    private MainController mainController;

    public EndScreenController(StackPane stackPane, MainController mainController) {
        this.stackPane = stackPane;
        this.mainController = mainController;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/endScreen/EndScreen.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void backButtonClicked(ActionEvent event) {
        stackPane.getChildren().clear();
        stackPane.getChildren().add(new MainviewController(stackPane,mainController));
    }

    @FXML
    void initialize() {
        this.prefWidthProperty().bind(stackPane.widthProperty());
        this.prefHeightProperty().bind(stackPane.heightProperty());

        playerOneText.setText(mainController.getGwent().getGame().getPlayer1().getPlayerName());
        playerTwoText.setText(mainController.getGwent().getGame().getPlayer2().getPlayerName());

        Map<Round, Pair<Integer, Integer>> scores = mainController.getGwent().getGame().getScoreMap();

        rOnePOneScore.setText(scores.get(Round.ONE).getValue0() + "");
        rTwoPOneScore.setText(scores.get(Round.TWO).getValue0() + "");
        rOnePTowScore.setText(scores.get(Round.ONE).getValue1() + "");
        rTwoPTwoScore.setText(scores.get(Round.TWO).getValue1() + "");

        boolean hasThird = false;

        if ((scores.get(Round.ONE).getValue0() > scores.get(Round.ONE).getValue1() && scores.get(Round.TWO).getValue0() < scores.get(Round.TWO).getValue1())
                || (scores.get(Round.ONE).getValue0() < scores.get(Round.ONE).getValue1() && scores.get(Round.TWO).getValue0() > scores.get(Round.TWO).getValue1())) {
            rThreePOneScore.setText(scores.get(Round.THREE).getValue0() + "");
            rThreePTwpScore.setText(scores.get(Round.THREE).getValue1() + "");
            hasThird = true;
        } else {
            rThreePOneScore.setText("");
            rThreePTwpScore.setText("");
        }

        int winnerCount = 0;
        if (hasThird) {
            winnerCount += mainController.getGwent().getGame().getWinnerMap().get(Round.ONE) == 2 ? -1 : mainController.getGwent().getGame().getWinnerMap().get(Round.ONE);
            winnerCount += mainController.getGwent().getGame().getWinnerMap().get(Round.TWO) == 2 ? -1 : mainController.getGwent().getGame().getWinnerMap().get(Round.TWO);
            winnerCount += mainController.getGwent().getGame().getWinnerMap().get(Round.THREE) == 2 ? -1 : mainController.getGwent().getGame().getWinnerMap().get(Round.THREE);
        } else {
            winnerCount += mainController.getGwent().getGame().getWinnerMap().get(Round.ONE) == 2 ? -1 : mainController.getGwent().getGame().getWinnerMap().get(Round.ONE);
            winnerCount += mainController.getGwent().getGame().getWinnerMap().get(Round.TWO) == 2 ? -1 : mainController.getGwent().getGame().getWinnerMap().get(Round.TWO);
        }

        if (winnerCount == 0) //TODO bild urls einfügen
            resultImage.setImage(new Image("")); //Draw
        else if ((winnerCount > 0 && mainController.getMultiplayerController().isHost )||(winnerCount < 0 && !mainController.getMultiplayerController().isHost ))
            resultImage.setImage(new Image("")); //Winner
        else
            resultImage.setImage(new Image("")); //Loser

    }
}
