package view.deckBuilder;

import controller.MainController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

import java.io.IOException;


public class NewDeckElementController extends BorderPane {

    @FXML
    private ImageView image;

    private MainController mainController;
    private StackPane stackPane;
    private DeckBuilderController deckBuilderController;

    public NewDeckElementController(StackPane stackPane, MainController mainController, DeckBuilderController deckBuilderController) {
        this.mainController = mainController;
        this.stackPane = stackPane;
        this.deckBuilderController = deckBuilderController;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/deckBuilder/NewDeckElement.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void newDeckClicked(MouseEvent event) { //TODO
        deckBuilderController.addNewDeck();
    }

    @FXML
    void initialize(){
        this.minWidthProperty().bind(deckBuilderController.getDeckList().widthProperty());
        this.minHeightProperty().bind(deckBuilderController.getDeckList().widthProperty().divide(4));
        image.setImage(new Image("/view/deckBuilder/newDeckImage.png"));
        image.fitHeightProperty().bind(deckBuilderController.getDeckList().widthProperty().divide(4));
        image.fitWidthProperty().bind(deckBuilderController.getDeckList().widthProperty());
    }

}

