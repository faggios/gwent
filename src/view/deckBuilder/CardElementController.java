package view.deckBuilder;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import model.*;
import javafx.scene.text.Text;

import java.io.IOException;

public class CardElementController extends BorderPane {

    @FXML
    private ImageView cardImage, marmor, factionBannerImage, pointsIcon, cardIcon, rowIcon, effectIcon;

    @FXML
    private Text cardAmountText, pointsText, cardNameText, cardNameTextTwo;

    @FXML
    private GridPane gridPane;

    private Card card;
    private DeckBuilderController deckBuilderController;
    private boolean isHero, hasPoints;
    private StackPane stackPane;
    private int columnCount, rowCOunt, amount;
    private FactionName factionName;


    public CardElementController(StackPane stackPane, DeckBuilderController deckBuilderController, Card card, int columnCount, int rowCount, FactionName factionName, int amount) {
        this.card = card;
        this.deckBuilderController = deckBuilderController;
        this.stackPane = stackPane;
        this.isHero = card.getTypes().contains(EffectType.HERO);
        this.hasPoints = card.getTypes().contains(EffectType.COMBAT);
        this.columnCount = columnCount;
        this.rowCOunt = rowCount;
        this.factionName = factionName;
        this.amount = amount;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/deckBuilder/CardElement.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Card: " + card.getCardName().toString());
        }
    }

    @FXML
    void initialize() {
        gridPane.setGridLinesVisible(true);
        this.prefHeightProperty().bind(deckBuilderController.getCardHeightBinding());
        this.prefWidthProperty().bind(deckBuilderController.getCardWidthBinding());
        gridPane.prefHeightProperty().bind(deckBuilderController.getCardHeightBinding());
        gridPane.prefWidthProperty().bind(deckBuilderController.getCardWidthBinding());


        String name = card.getCardName().toString().replace('_', ' '); //TODO in 2 textfelder
        String name1 = "", name2 = "";

        if (name.length() > 12) {
            int count = 0;
            String[] split = name.split(" ");
            if (split.length > 1) {
                for (int i = 0; i < split.length; i++) {
                    count += split[i].length();
                    if (count + i > 12 && i > 0) {
                        count = i;
                        break;
                    }
                }
                for (int i = 0; i < count; i++) {
                    name1 = name1.concat(split[i] + " ");
                }
                for (int i = count; i < split.length; i++) {
                    name2 = name2.concat(split[i] + " ");
                }
                cardNameText.setText(name1);
                cardNameTextTwo.setText(name2);
            }else cardNameText.setText(name1);
        } else cardNameText.setText(name);

        switch (factionName) {
            case SKELLIGE:
                factionBannerImage.setImage(new Image("/assets/card_extra/banner/skellige1.png"));
                break;
            case MONSTER:
                factionBannerImage.setImage(new Image("/assets/card_extra/banner/monster1.png"));
                break;
            case NORTHERN_REALMS:
                factionBannerImage.setImage(new Image("/assets/card_extra/banner/north1.png"));
                break;
            case NILFGAARDIAN_EMPIRE:
                factionBannerImage.setImage(new Image("/assets/card_extra/banner/nilf1.png"));
                break;
            case SCOIATAEL:
                factionBannerImage.setImage(new Image("/assets/card_extra/banner/scoiatel1.png"));
                break;
            default:
                System.out.println("Error: Keine Fraktion ausgewählt");
                break;
        }

        //cardImage.fitHeightProperty().bind(deckBuilderController.getCardHeightBinding().multiply(0.74));
        cardImage.fitWidthProperty().bind(deckBuilderController.getCardWidthBinding());
        //marmor.fitHeightProperty().bind(deckBuilderController.getCardHeightBinding().multiply(0.26));
        marmor.fitWidthProperty().bind(deckBuilderController.getCardWidthBinding());

        pointsIcon.fitWidthProperty().bind(deckBuilderController.getCardWidthBinding().multiply(0.56914));
        pointsIcon.fitHeightProperty().bind(deckBuilderController.getCardHeightBinding().multiply(0.30225));

        factionBannerImage.fitWidthProperty().bind(deckBuilderController.getCardWidthBinding().multiply(0.205));
        //factionBannerImage.fitHeightProperty().bind(deckBuilderController.getCardHeightBinding().multiply(0.30225));

        cardIcon.fitWidthProperty().bind(deckBuilderController.getCardWidthBinding().multiply(0.1));
        cardIcon.fitHeightProperty().bind(deckBuilderController.getCardHeightBinding().multiply(0.1));
        rowIcon.fitWidthProperty().bind(deckBuilderController.getCardWidthBinding().multiply(0.24468));
        //rowIcon.fitHeightProperty().bind(deckBuilderController.getCardHeightBinding().multiply(0.88));
        effectIcon.fitWidthProperty().bind(deckBuilderController.getCardWidthBinding().multiply(0.24468));
        //effectIcon.fitHeightProperty().bind(deckBuilderController.getCardHeightBinding().multiply(0.88));


        cardImage.setImage(new Image("/assets/cards/easy/" + card.getCardName().toString() + ".png"));
        marmor.setImage(new Image("/assets/card_extra/mamor.png"));
        if (card.getTypes().contains(EffectType.COMBAT)) {
            pointsText.setText(card.getDefaultPoints() + "");
            if (card.getTypes().contains(EffectType.HERO)) {
                pointsText.setFill(new Color(1, 0.9, 0.9, 1));
                pointsIcon.setImage(new Image("/assets/card_extra/topIcons/hero.png"));
            } else
                pointsIcon.setImage(new Image("/assets/card_extra/topIcons/normal.png"));

            if (card.getPlace(Place.MELEE) && card.getPlace(Place.RANGED))
                rowIcon.setImage(new Image("/assets/card_extra/rowIcons/melee_ranged.png"));
            else if (card.getPlace(Place.MELEE))
                rowIcon.setImage(new Image("/assets/card_extra/rowIcons/melee.png"));
            else if (card.getPlace(Place.RANGED))
                rowIcon.setImage(new Image("/assets/card_extra/rowIcons/ranged.png"));
            else
                rowIcon.setImage(new Image("/assets/card_extra/rowIcons/artillery.png"));

            if (card.getTypes().contains(EffectType.SPY))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/spy.png"));
            else if (card.getCardName().equals(CardName.Dandelion))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/boost.png"));
            else if (card.getTypes().contains(EffectType.MORALEBOOST))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/morale.png"));
            else if (card.getTypes().contains(EffectType.MARDROEME))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/mardroeme.png"));
            else if (card.getTypes().contains(EffectType.BERSERKER))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/berserk.png"));
            else if (card.getTypes().contains(EffectType.MEDIC))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/healer.png"));
            else if (card.getCardName().equals(CardName.Clan_Dimun_Pirate))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/fullScorch.png"));
            else if (card.getTypes().contains(EffectType.SCORCH))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/scorch.png"));
            else if (card.getTypes().contains(EffectType.TIGHTBOND))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/bond.png"));
            else if (card.getTypes().contains(EffectType.SUMMONAVANGER))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/avenger.png"));
            else if (card.getTypes().contains(EffectType.MUSTER))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/summon.png"));
            else if (card.getTypes().contains(EffectType.AGILE))
                effectIcon.setImage(new Image("/assets/card_extra/effectIcons/swap.png"));

        } else {
            switch (card.getCardName()) {
                case Clear_Weather:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/clear.png"));
                    break;
                case Decoy:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/decoy.png"));
                    break;
                case Impenetrable_Fog:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/fog.png"));
                    break;
                case Biting_Frost:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/frost.png"));
                    break;
                case Commanders_Horn:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/horn.png"));
                    break;
                case Mardroeme:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/mardroeme.png"));
                    break;
                case Torrential_Rain:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/rain.png"));
                    break;
                case Scorch:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/scorch.png"));
                    break;
                case Skellige_Storm:
                    pointsIcon.setImage(new Image("/assets/card_extra/topIcons/storm.png"));
                    break;
            }
        }
        cardIcon.setImage(new Image("/assets/card_extra/cardIcon.png"));

        refresh();
    }

    public boolean isHero() {
        return isHero;
    }

    @FXML
    void cardPressed(MouseEvent event) { //TODO anzeigen das ausgewählt
        deckBuilderController.setSelectedCard(this);
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    public int getRowCount() {
        return rowCOunt;
    }

    public void setRowCount(int rowCOunt) {
        this.rowCOunt = rowCOunt;
    }

    public void refresh() {
        cardAmountText.setText("x" + amount);
    } //TODO richtige größe + stelle

    public void setAmount(int amount) {
        this.amount = amount;
        refresh();
    }

    public int getAmount() {
        return amount;
    }

    public Card getCard() {
        return card;
    }
}

