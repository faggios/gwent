package view.deckBuilder;

import controller.MainController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import model.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SelectLeaderController extends BorderPane { //TODO style festlegen und gucken warum unsichtbar

    @FXML
    private BorderPane borderPane;

    @FXML
    private ImageView imageView1,imageView2,imageView3,imageView4,imageView5;

    @FXML
    private Button selectButton,returnButton;

    private StackPane stackPane;
    private MainController mainController;
    List<LeaderName> list;
    private int selected;
    private List<ImageView> images;
    private Deck deck;
    private DeckBuilderController deckBuilderController;

    public SelectLeaderController(StackPane stackPane, MainController mainControlle, List<LeaderName> list, Deck deck,DeckBuilderController deckBuilderController) {
        this.stackPane = stackPane;
        this.mainController = mainControlle;
        this.list = list; // can be null
        this.deck = deck;
        this.deckBuilderController = deckBuilderController;
        selected = 0;
        images = new ArrayList<>();
        images.add(imageView1);
        images.add(imageView2);
        images.add(imageView3);
        images.add(imageView4);
        images.add(imageView5);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/deckBuilder/SelectLeaderView.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize() {
        this.prefHeightProperty().bind(stackPane.heightProperty());
        this.prefWidthProperty().bind(stackPane.widthProperty());

        imageView1.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.3));
        //imageView1.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.44));
        imageView2.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.35));
        //imageView2.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.44));
        imageView3.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.5));
        //imageView3.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.44));
        imageView4.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.35));
        //imageView4.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.44));
        imageView5.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.3));
        //imageView5.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.44));

        refreshImages();
    }

    @FXML
    void hasScrolled(ScrollEvent event) {
        if (event.getDeltaY() < 0) { //down i think
            if (selected > 0) {
                selected--;
                refreshImages();
            }
        } else if (event.getDeltaY() > 0) { //up i think
            if (selected < list.size() - 1) {
                selected++;
                refreshImages();
            }
        }
    }

    @FXML
    void returnClicked(ActionEvent event) {
        stackPane.getChildren().remove(this);
    }

    @FXML
    void selectClicked(ActionEvent event) {
        deckBuilderController.setSelectedLeader(Leader.buildLeader(list.get(selected)));
        stackPane.getChildren().remove(this);
    }

    void refreshImages(){
        if(selected==0){
            imageView1.setImage(null);
            imageView2.setImage(null);
            imageView3.setImage(new Image("assets/cards/easy/"+list.get(0).toString()+".png"));
            imageView4.setImage(new Image("assets/cards/easy/"+list.get(1).toString()+".png"));
            if(list.size()>2) imageView5.setImage(new Image("assets/cards/easy/"+list.get(2).toString()+".png"));
            else imageView5.setImage(null);
        }else if(selected==1){
            imageView1.setImage(null);
            imageView2.setImage(new Image("assets/cards/easy/"+list.get(0).toString()+".png"));
            imageView3.setImage(new Image("assets/cards/easy/"+list.get(1).toString()+".png"));
            if(list.size()>2) imageView4.setImage(new Image("assets/cards/easy/"+list.get(2).toString()+".png"));
            else imageView4.setImage(null);
            if(list.size()>3) imageView5.setImage(new Image("assets/cards/easy/"+list.get(3).toString()+".png"));
            else imageView5.setImage(null);
        }else if(selected==2){
            imageView1.setImage(new Image("assets/cards/easy/"+list.get(0).toString()+".png"));
            imageView2.setImage(new Image("assets/cards/easy/"+list.get(1).toString()+".png"));
            imageView3.setImage(new Image("assets/cards/easy/"+list.get(2).toString()+".png"));
            if(list.size()>3) imageView4.setImage(new Image("assets/cards/easy/"+list.get(3).toString()+".png"));
            else imageView4.setImage(null);
            if(list.size()>4) imageView5.setImage(new Image("assets/cards/easy/"+list.get(4).toString()+".png"));
            else imageView5.setImage(null);
        }else if(selected==3){
            imageView1.setImage(new Image("assets/cards/easy/"+list.get(1).toString()+".png"));
            imageView2.setImage(new Image("assets/cards/easy/"+list.get(2).toString()+".png"));
            imageView3.setImage(new Image("assets/cards/easy/"+list.get(3).toString()+".png"));
            if(list.size()>4) imageView4.setImage(new Image("assets/cards/easy/"+list.get(4).toString()+".png"));
            else imageView4.setImage(null);
            if(list.size()>5) imageView5.setImage(new Image("assets/cards/easy/"+list.get(5).toString()+".png"));
            else imageView5.setImage(null);
        }else if(selected==4){
            imageView1.setImage(new Image("assets/cards/easy/"+list.get(2).toString()+".png"));
            imageView2.setImage(new Image("assets/cards/easy/"+list.get(3).toString()+".png"));
            imageView3.setImage(new Image("assets/cards/easy/"+list.get(4).toString()+".png"));
            if(list.size()>5) imageView4.setImage(new Image("assets/cards/easy/"+list.get(5).toString()+".png"));
            else imageView4.setImage(null);
            if(list.size()>6) imageView5.setImage(new Image("assets/cards/easy/"+list.get(6).toString()+".png"));
            else imageView5.setImage(null);
        }
    }
}
