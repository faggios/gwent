package view.deckBuilder;

import controller.MainController;
import javafx.beans.binding.DoubleBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.*;
import java.io.IOException;
import java.util.*;

public class DeckBuilderController extends BorderPane {

    @FXML
    private ScrollPane allCardListScrollbar,addedCardListSCrollbar;

    @FXML
    private AnchorPane allCardsAnchor,addedCardsAnchor;

    @FXML
    private VBox deckList;

    @FXML
    private Button insertToDeckButton, removeFromDeckButton;

    @FXML
    private Text cardAmountText,specialCardAmountText,normalCardAmountText,totalStrengthText,heroCardsText;

    @FXML
    private TextField deckNameField;

    @FXML
    private ImageView faction1Image, faction2Image, faction3Image, faction4Image, faction5Image, leaderImage,deleteImage;

    @FXML
    private GridPane allCardList,addedCardList;

    private MainController mainController;
    private StackPane stackPane;
    private CardElementController selectedCard;
    private List<CardElementController> monsterCards, scojatelCards, northCards, nilfCards, skelligeCards, addedCards;
    private DeckElementController selectedDeck;
    private int specialCardCount = 0;
    private DoubleBinding cardWidth,cardHeight;

    public DeckBuilderController(StackPane stackPane, MainController mainController) {
        this.mainController = mainController;
        this.stackPane = stackPane;
        selectedDeck = null;
        monsterCards = new ArrayList<>();
        scojatelCards = new ArrayList<>();
        northCards = new ArrayList<>();
        nilfCards = new ArrayList<>();
        skelligeCards = new ArrayList<>();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/deckBuilder/DeckBuilder.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void refreshAllCardList(){
        allCardList.getChildren().clear();
        if(selectedDeck==null)return;
        int rowCount = 0;
        int columnCount = 0;
        List<CardName> cloned = new ArrayList<>(selectedDeck.getDeck().getFaction().getCards());
        cloned.addAll(mainController.getGwent().getFaction(FactionName.NEUTRAL).getCards());
        for(Card card : selectedDeck.getDeck().getCards()){
            cloned.remove(card.getCardName());
        }
        allCardList.addRow(0);
        for(CardName ce : cloned){ //TODO: amount
            allCardList.add(new CardElementController(stackPane,this,Card.buildCard(ce),columnCount,rowCount,selectedDeck.getDeck().getFaction().getName(),1),columnCount,rowCount);
            if((columnCount+1)%3==0){
                columnCount = 0;
                rowCount++;
                allCardList.addRow(rowCount);
            }else
                columnCount++;
        }
    }

    void refreshAddedCardList(){
        addedCardList.getChildren().clear();
        if(selectedDeck==null)return;
        int rowCount = 0;
        int columnCount = 0;
        for(Card c : selectedDeck.getDeck().getCards()){
            addedCardList.add(new CardElementController(stackPane,this,c,columnCount,rowCount,selectedDeck.getDeck().getFaction().getName(),1),columnCount,rowCount);
            if((columnCount+1)%3==0){
                columnCount = 0;
                rowCount++;
                addedCardList.addRow(rowCount);
            }else
                columnCount++;
        }
    }

    public void deckSelected(DeckElementController deckElementController) {
        if (deckElementController != null) {
            selectedDeck = deckElementController;
            refreshAddedCardList();
            refreshAllCardList();
            refreshCardCount();
            refreshLeader();
        }
    }

    public void refreshCardCount(){
        if(selectedDeck==null)return;
        int specialCardCount = 0;
        for(Card c : selectedDeck.getDeck().getCards()){
            if(!c.getTypes().contains(EffectType.COMBAT))
                specialCardCount++;
        }
        totalStrengthText.setText(selectedDeck.getDeck().getTotalPoints()+"");
        heroCardsText.setText(selectedDeck.getDeck().getTotalHeros()+"");
        cardAmountText.setText(selectedDeck.getDeck().getCards().size()+"");
        normalCardAmountText.setText((selectedDeck.getDeck().getCards().size()-specialCardCount)+"/20");
        if(selectedDeck.getDeck().getCards().size()-specialCardCount>=20)
            normalCardAmountText.setFill(new Color(0,1,0,1)); //grün
        else
            normalCardAmountText.setFill(new Color(1,0,0,1)); //rot

        specialCardAmountText.setText(specialCardCount+"/10");
        if(specialCardCount<10)
            specialCardAmountText.setFill(new Color(0,1,0,1)); //grün
        else
            specialCardAmountText.setFill(new Color(1,0,0,1)); //rot

        this.specialCardCount = specialCardCount;
    }

    @FXML
    void initialize() {

        this.minWidthProperty().bind(stackPane.widthProperty());
        this.minHeightProperty().bind(stackPane.heightProperty());

        deleteImage.setImage(new Image("/assets/icons/Unbenannt.png"));


        deckList.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.2));
        deckList.prefHeightProperty().bind(stackPane.heightProperty());

        allCardListScrollbar.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.8).multiply(0.41));
        addedCardListSCrollbar.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.8).multiply(0.41));
        allCardListScrollbar.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.79));
        addedCardListSCrollbar.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.79));

        allCardsAnchor.prefWidthProperty().bind(allCardListScrollbar.widthProperty());
        allCardsAnchor.prefHeightProperty().bind(allCardListScrollbar.heightProperty());
        addedCardsAnchor.prefWidthProperty().bind(addedCardListSCrollbar.widthProperty());
        addedCardsAnchor.prefHeightProperty().bind(addedCardListSCrollbar.heightProperty());


        allCardList.prefWidthProperty().bind(allCardsAnchor.widthProperty());
        allCardList.prefHeightProperty().bind(allCardsAnchor.heightProperty());
        addedCardList.prefWidthProperty().bind(addedCardsAnchor.widthProperty());
        addedCardList.prefHeightProperty().bind(addedCardsAnchor.heightProperty());

        cardWidth = stackPane.widthProperty().multiply(0.098).multiply(0.9);
        cardHeight = cardWidth.multiply(1.883);

        faction1Image.setImage(new Image("/assets/icons/icon_monster_1.png"));
        faction2Image.setImage(new Image("/assets/icons/icon_nilfgaard.png"));
        faction3Image.setImage(new Image("/assets/icons/icon_northernRealms.png"));
        faction4Image.setImage(new Image("/assets/icons/icon_scoiatel.png"));
        faction5Image.setImage(new Image("/assets/icons/icon_skellige.png"));

        leaderImage.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.067));
        leaderImage.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.223));

        boolean firstDeck = true;
        for (Deck deck : mainController.getGwent().getDecks()) {
            if(!firstDeck)
                deckList.getChildren().add(new DeckElementController(stackPane, mainController, deck, this));
            else {
                firstDeck = false;
                DeckElementController d = new DeckElementController(stackPane, mainController, deck, this);
                deckList.getChildren().add(d);
                selectedDeck = d;
                d.refresh();
                selectedDeck.resetFaction();
            }
        }
        deckList.getChildren().add(new NewDeckElementController(stackPane,mainController,this));

        refreshLeader();
        refreshAllCardList();
        refreshAddedCardList();
        refreshCardCount();
    }

    public DoubleBinding getCardHeightBinding() { return cardHeight; }
    public DoubleBinding getCardWidthBinding() { return cardWidth; }

    public void refreshLeader(){
        if(selectedDeck != null)
            leaderImage.setImage(new Image("/assets/cards/easy/"+selectedDeck.getDeck().getLeader().getName()+".png"));
        else
            leaderImage.setImage(null);
    }

    public void setSelectedLeader(Leader leader) {
        if(selectedDeck!=null)
            selectedDeck.getDeck().setLeader(leader);
        refreshLeader();
    }

    @FXML
    void deleteImagePressed(MouseEvent event) {
        if(selectedDeck == null) return;
        deckList.getChildren().remove(selectedDeck);
        mainController.getIoController().deleteDeckFile(selectedDeck.getDeck().getUniqueID());
        mainController.getGwent().getDecks().remove(selectedDeck.getDeck());
        if(deckList.getChildren().size()==1){
            selectedDeck = null;
        }else{
            selectedDeck = (DeckElementController) deckList.getChildren().get(0);
        }
        refreshLeader();
        refreshAllCardList();
        refreshAddedCardList();
        refreshCardCount();
    }

    public void setSelectedCard(CardElementController cardElementController) { selectedCard = cardElementController; }

    public void addNewDeck(){
        Deck deck = new Deck();
        deck.setDeckName("New Deck");
        deck.setFaction(mainController.getGwent().getFaction(FactionName.MONSTER));
        deck.setLeader(Leader.buildLeader(mainController.getGwent().getFaction(FactionName.MONSTER).getLeaders().get(0)));
        deck.setUsable(false);
        selectedDeck = new DeckElementController(stackPane,mainController,deck,this);
        deckList.getChildren().add(deckList.getChildren().size()-1, selectedDeck);
        mainController.getGwent().getDecks().add(deck);
        refreshCardCount();
        refreshAllCardList();
        refreshAddedCardList();
        refreshLeader();
        selectedDeck.refresh();
        selectedDeck.resetFaction();
    }

    @FXML
    void deckNameTyped(KeyEvent event) {
        if(selectedDeck!=null) {
            selectedDeck.getDeck().setDeckName(deckNameField.getText());
            selectedDeck.refresh();
        }
    }

    @FXML
    void faction1ImagePressed(MouseEvent event) {//TODO view ändern ?
        selectedDeck.getDeck().setFaction(mainController.getGwent().getFaction(FactionName.MONSTER));
        selectedDeck.getDeck().setLeader(Leader.buildLeader(mainController.getGwent().getFaction(FactionName.MONSTER).getLeaders().get(0)));
        selectedDeck.getDeck().getCards().clear();
        refreshCardCount();
        refreshAllCardList();
        refreshAddedCardList();
        refreshLeader();
        selectedDeck.refresh();
        selectedDeck.resetFaction();
    }

    @FXML
    void faction2ImagePressed(MouseEvent event) {//TODO view ändern ?
        selectedDeck.getDeck().setFaction(mainController.getGwent().getFaction(FactionName.NILFGAARDIAN_EMPIRE));
        selectedDeck.getDeck().setLeader(Leader.buildLeader(mainController.getGwent().getFaction(FactionName.NILFGAARDIAN_EMPIRE).getLeaders().get(0)));
        selectedDeck.getDeck().getCards().clear();
        refreshCardCount();
        refreshAllCardList();
        refreshAddedCardList();
        refreshLeader();
        selectedDeck.refresh();
        selectedDeck.resetFaction();
    }

    @FXML
    void faction3ImagePressed(MouseEvent event) {//TODO view ändern ?
        selectedDeck.getDeck().setFaction(mainController.getGwent().getFaction(FactionName.NORTHERN_REALMS));
        selectedDeck.getDeck().setLeader(Leader.buildLeader(mainController.getGwent().getFaction(FactionName.NORTHERN_REALMS).getLeaders().get(0)));
        selectedDeck.getDeck().getCards().clear();
        refreshCardCount();
        refreshAllCardList();
        refreshAddedCardList();
        refreshLeader();
        selectedDeck.refresh();
        selectedDeck.resetFaction();
    }

    @FXML
    void faction4ImagePressed(MouseEvent event) {//TODO view ändern ?
        selectedDeck.getDeck().setFaction(mainController.getGwent().getFaction(FactionName.SCOIATAEL));
        selectedDeck.getDeck().setLeader(Leader.buildLeader(mainController.getGwent().getFaction(FactionName.SCOIATAEL).getLeaders().get(0)));
        selectedDeck.getDeck().getCards().clear();
        refreshCardCount();
        refreshAllCardList();
        refreshAddedCardList();
        refreshLeader();
        selectedDeck.refresh();
        selectedDeck.resetFaction();
    }

    @FXML
    void faction5ImagePressed(MouseEvent event) { //TODO view ändern ?
        selectedDeck.getDeck().setFaction(mainController.getGwent().getFaction(FactionName.SKELLIGE));
        selectedDeck.getDeck().setLeader(Leader.buildLeader(mainController.getGwent().getFaction(FactionName.SKELLIGE).getLeaders().get(0)));
        selectedDeck.getDeck().getCards().clear();
        selectedDeck.getDeck().setTotalPoints(0);
        refreshCardCount();
        refreshAllCardList();
        refreshAddedCardList();
        refreshLeader();
        selectedDeck.refresh();
        selectedDeck.resetFaction();
    }

    @FXML
    void leaderImagePressed(MouseEvent event) {
        stackPane.getChildren().add(new SelectLeaderController(stackPane, mainController, selectedDeck.getDeck().getFaction().getLeaders(), selectedDeck.getDeck(), this));
    }

    @FXML
    void insertToDeckPressed(ActionEvent event) {
        if(selectedCard!=null&&specialCardCount<10){
            selectedDeck.getDeck().getCards().add(selectedCard.getCard());
            selectedDeck.getDeck().setTotalPoints(selectedDeck.getDeck().getTotalPoints()+selectedCard.getCard().getDefaultPoints());
            if(selectedCard.isHero())
                selectedDeck.getDeck().setTotalHeros(selectedDeck.getDeck().getTotalHeros()+1);
            selectedCard = null;

            refreshAddedCardList();
            refreshAllCardList();
            refreshCardCount();
            selectedDeck.refresh();
        }
    }

    @FXML
    void removeFromDeckClicked(ActionEvent event) {
        if(selectedCard!=null){
            selectedDeck.getDeck().getCards().remove(selectedCard.getCard());
            selectedDeck.getDeck().setTotalPoints(selectedDeck.getDeck().getTotalPoints()-selectedCard.getCard().getDefaultPoints());
            if(selectedCard.isHero())
                selectedDeck.getDeck().setTotalHeros(selectedDeck.getDeck().getTotalHeros()-1);
            selectedCard = null;

            refreshAddedCardList();
            refreshAllCardList();
            refreshCardCount();
            selectedDeck.refresh();
        }
    }

    public VBox getDeckList() { return deckList; }

}
