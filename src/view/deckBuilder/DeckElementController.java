package view.deckBuilder;

import controller.MainController;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.Card;
import model.Deck;
import model.EffectType;

import java.io.IOException;

public class DeckElementController extends BorderPane {

    @FXML
    private Text deckNameText;
    @FXML
    private ImageView factionImage;
    @FXML
    private Text cardsText;
    @FXML
    private GridPane gridPane;

    private MainController mainController;
    private StackPane stackPane;
    private Deck deck;
    private DeckBuilderController deckBuilderController;
    private Color green,red;

    public DeckElementController(StackPane stackPane, MainController mainController, Deck deck,DeckBuilderController deckBuilderController) {
        this.mainController = mainController;
        this.stackPane = stackPane;
        this.deck = deck;
        this.deckBuilderController = deckBuilderController;
        green = new Color(0.19,0.8,0.19,1);
        red = new Color(1,0,0,1);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/deckBuilder/DeckElement.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize(){
        this.prefWidthProperty().bind(deckBuilderController.getDeckList().widthProperty());
        this.prefHeightProperty().bind(stackPane.widthProperty().multiply(0.05));
        factionImage.fitHeightProperty().bind(this.heightProperty());
        refresh();
    }

    public void resetFaction(){
        switch(deck.getFaction().getName()){
            case MONSTER: factionImage.setImage(new Image("/assets/icons/icon_monster_1.png"));   break;
            case SKELLIGE: factionImage.setImage(new Image("/assets/icons/icon_skellige.png"));  break;
            case SCOIATAEL: factionImage.setImage(new Image("/assets/icons/icon_scoiatel.png")); break;
            case NORTHERN_REALMS: factionImage.setImage(new Image("/assets/icons/icon_northernRealms.png"));   break;
            case NILFGAARDIAN_EMPIRE: factionImage.setImage(new Image("/assets/icons/icon_nilfgaard.png"));   break;
            default: System.out.println("Error: deck has no faction");
        }
    }

    public void refresh(){
        resetFaction();
        deckNameText.setText(deck.getName());

        int normalCardsAmount = 0;
        for(Card c : deck.getCards()){
            if(c.getTypes().contains(EffectType.COMBAT))
                normalCardsAmount++;
        }
        cardsText.setText("Cards: "+ normalCardsAmount + " | Special: " + (deck.getCards().size()-normalCardsAmount));

        if(deck.getCards().size()-normalCardsAmount<=10 && normalCardsAmount >= 20) {
            cardsText.setFill(green);
            deck.setUsable(true);
        }
        else {
            cardsText.setFill(red);
            deck.setUsable(false);
        }
    }
    @FXML
    void deckSelected(MouseEvent event) {
        deckBuilderController.deckSelected(this);
    }

    public Deck getDeck(){
        return deck;
    }
}
