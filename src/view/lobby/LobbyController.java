package view.lobby;

import controller.MainController;
import controller.MultiplayerController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import model.*;
import view.gameboard.GameBoardController;
import view.mainview.MainviewController;

import java.io.IOException;

public class LobbyController extends BorderPane {

    @FXML
    private BorderPane root;

    @FXML
    private Button leaveLobbyButton;

    @FXML
    private Label ipAddressLabel;

    @FXML
    private Label portLabel;

    @FXML
    private ComboBox<String> playerOneDeckComboBox;

    @FXML
    private Text player1Text;

    @FXML
    private Text player2Text;

    @FXML
    private Button readyButton;

    private MainController mainController;
    private StackPane stackPane;
    private boolean isHost;
    private boolean isReady;
    private String playername;

    public LobbyController(MainController mainController, StackPane stackPane, Boolean isHost, String playername) {
        this.mainController = mainController;
        this.stackPane = stackPane;
        this.isHost = isHost;
        this.isReady = false;
        this.playername = playername;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/lobby/Lobby.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize() {
        for (Deck d : mainController.getGwent().getDecks()) {
            if (d.isUsable())
                playerOneDeckComboBox.getItems().add(d.getName());
        }
        if (isHost) {
            player1Text.setText("Player 1 (Host): " + playername);
            player2Text.setText("Player 2: ");
            ipAddressLabel.setText("IP-Address: " + mainController.getMultiplayerController().getIP());
            portLabel.setText("Port: 7777");
        } else {
            player1Text.setText("Player 1 (Host): ");
            player2Text.setText("Player 2: " + playername);
        }
    }

    public String getPlayername() {
        return playername;
    }

    public void playerJoined(String s) {
        if (isHost) player2Text.setText("Player 2: " + s);
         else player1Text.setText("Player 1: " + s);
    }

    @FXML
    void readyClicked(ActionEvent event) { //TODO rework together with startGame in gameController
        if (isReady) return;
        isReady = true;

        readyButton.setDisable(true);
        playerOneDeckComboBox.setDisable(true);
        Deck selectedDeck = null;
        for (Deck d : mainController.getGwent().getDecks()) {
            if (d.getName().equals(playerOneDeckComboBox.getValue())) {
                selectedDeck = d;
                break;
            }
        }
        if (selectedDeck == null) {
            System.out.println("No Deck selected");
            return;
        }
        System.out.println("Deck selected");
        Player playerMe = new Player(playername, selectedDeck.getFaction(), Leader.buildLeader(selectedDeck.getLeader().getName()), selectedDeck);
        Player otherP;
        if (MultiplayerController.isHost) {
            otherP = mainController.getMultiplayerController().recievePlayer();
            mainController.getMultiplayerController().send(playerMe);
        } else {
            mainController.getMultiplayerController().send(playerMe);
            otherP = mainController.getMultiplayerController().recievePlayer();
        }
        System.out.println("Player send and received");

        try {
            System.out.println("name: " + otherP.getPlayerName());
        } catch (NullPointerException e) {
        }
        try {
            System.out.println("faction: " + otherP.getFaction().getName().toString());
        } catch (NullPointerException e) {
        }
        try {
            System.out.println("Leader: " + otherP.getLeader().getName().toString());
        } catch (NullPointerException e) {
        }
        try {
            System.out.println("deck: " + otherP.getDeck().getName());
        } catch (NullPointerException e) {
        }
        System.out.println("Information has been printed");

        stackPane.getChildren().remove(this);

        Game newGame = new Game();
        mainController.getGwent().setGame(newGame);
        mainController.getGwent().getGame().setRound(Round.SETUP);

        newGame.setPlayer1(isHost ? playerMe : otherP);//Hostplayer
        newGame.setPlayer2(isHost ? otherP : playerMe);//Clientplayer

        //gameboard
        GameBoardController gameBoardController = new GameBoardController(mainController, mainController.getStackPane(), MultiplayerController.isHost);
        mainController.setGameBoardController(gameBoardController);
        stackPane.getChildren().add(gameBoardController);

        mainController.getGameController().startGame(isHost ? playerMe : otherP, isHost ? otherP : playerMe, newGame);

    }

    @FXML
    void leaveLobbyPressed(ActionEvent event) { //TODO connection schließen
        mainController.getMultiplayerController().stop();
        stackPane.getChildren().remove(this);
        stackPane.getChildren().add(new MainviewController(stackPane, mainController));
    }
}


