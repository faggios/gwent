package view.selectCardView;

import controller.MainController;
import controller.MultiplayerController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import model.Card;
import model.Deck;
import model.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

public class SelectCardController extends BorderPane {

    @FXML
    private BorderPane borderPane;

    @FXML
    private ImageView imageView1,imageView2,imageView3,imageView4,imageView5;

    @FXML
    private Button selectButton,returnButton;

    private StackPane stackPane;
    private MainController mainController;
    /**
     * this int determines where this view is beign called from
     * 1 = (Leader) EredinKing Any Weather card from drawPile
     * 2 = (Leader) EredinDeath restore Card from discardPile to hand
     * 3 = (Card) Medic Revive view
     * 4 = (Leader) EmhyrRelentless select card from enemy discard pile to add to your handcards
     * 5 = (Leader) eredinWorld discard 2 cards and choose one to add to your handcards from your deck list = handcards,list2=discard,list3=draw
     * 6 = (Card) selectCard to redraw
     * 7 = (Card) discardPile
     */
    private int calledFrom;
    private Player player;
    List<Card> list;
    private int selected;
    private List<ImageView> images;
    private boolean viewOnly,secondCardDiscarded;
    private int eredinWorldCount;
    private Deck deck;

    public SelectCardController(StackPane stackPane, MainController mainController, int calledFrom, Player player, List<Card> list, boolean viewOnly, Deck deck) {
        this.stackPane = stackPane;
        this.mainController = mainController;
        this.calledFrom = calledFrom;
        this.player = player;
        this.list = list; // can be null
        this.viewOnly = viewOnly;
        this.deck = deck;
        eredinWorldCount = 0;
        secondCardDiscarded = false;
        selected = 0;
        images = new ArrayList<>();
        images.add(imageView1);
        images.add(imageView2);
        images.add(imageView3);
        images.add(imageView4);
        images.add(imageView5);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/selectCardView/SelectCardView.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize() {
        this.prefHeightProperty().bind(stackPane.heightProperty());
        this.prefWidthProperty().bind(stackPane.widthProperty());
        System.out.println("Initiating the select screen");
        if (viewOnly) {
            selectButton.setVisible(false);
            selectButton.setDisable(true);
            returnButton.setVisible(true);
            returnButton.setDisable(false);
        } else {
            returnButton.setVisible(false);
            returnButton.setDisable(true);
            selectButton.setVisible(true);
            selectButton.setDisable(false);
        }

        imageView1.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.3));
        imageView2.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.35));
        imageView3.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.5));
        imageView4.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.35));
        imageView5.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.328).multiply(0.3));

        refreshImages();
    }

    void eredinKing() {
        mainController.getPlayerController().playCard(player, player, list.get(selected), null, null, list, false);
        stackPane.getChildren().remove(this);
    }

    void eredinDeath() {
        player.getHandcards().add(list.remove(selected));
        stackPane.getChildren().remove(this);
    }

    void medicCard() {
        mainController.getPlayerController().playCard(player, player, list.get(selected), null, null, list, false);
        stackPane.getChildren().remove(this);
    }

    void emhyrRelentless() {
        player.getHandcards().add(list.remove(selected));
        stackPane.getChildren().remove(this);
    }

    void eredinWorld() {
        switch (eredinWorldCount) {
            case 1:
                player.getDiscardPile().add(list.remove(selected));
                selected--;
                refreshImages();
                break;
            case 2:
                player.getDiscardPile().add(list.remove(selected));
                this.list = player.getDrawPile();
                selected = 0;
                refreshImages();
                break;
            case 3:
                list.add(player.getDrawPile().remove(selected));
                break;
        }
    }

    void redrawCard(){
        if(!secondCardDiscarded){
            Card c = player.getHandcards().remove(selected);
            player.getHandcards().add(selected,player.getDrawPile().get(new Random().nextInt(player.getDrawPile().size())));
            player.getDrawPile().add(c);
            imageView3.setImage(new Image("/assets/cards/easy/" + list.get(selected).getCardName() + ".png"));
            secondCardDiscarded = true;
        }else{
            Card c = player.getHandcards().remove(selected);
            player.getHandcards().add(selected,player.getDrawPile().get(new Random().nextInt(player.getDrawPile().size())));
            player.getDrawPile().add(c);
            imageView3.setImage(new Image("/assets/cards/easy/" + list.get(selected).getCardName() + ".png"));
            selectButton.setDisable(true);
            returnButton.setDisable(true);
            if(MultiplayerController.isHost){
                mainController.getMultiplayerController().send(10);
                if(10!=mainController.getMultiplayerController().recieveInt()){
                    System.out.println("Error in connection after redrawing cards in 163");
                }
                mainController.getGameController().continueStartGame();
            }
            else{
                if(10!=mainController.getMultiplayerController().recieveInt()){
                    System.out.println("Error in connection after redrawing cards in 168");
                }
                mainController.getMultiplayerController().send(10);
                mainController.getGameController().continueStartGame();
            }
            stackPane.getChildren().remove(this);
        }
    }

    @FXML
    void hasScrolled(ScrollEvent event) {
        if (event.getDeltaY() < 0) { //down i think
            if (selected > 0) {
                selected--;
                refreshImages();
            }
        } else if (event.getDeltaY() > 0) { //up i think
            if (selected < list.size() - 1) {
                selected++;
                refreshImages();
            }
        }
    }

    @FXML
    void returnClicked(ActionEvent event) {
        stackPane.getChildren().remove(this);
    }

    @FXML
    void selectClicked(ActionEvent event) {
        switch (calledFrom) {
            case 1:
                eredinKing();
                break;
            case 2:
                eredinDeath();
                break;
            case 3:
                medicCard();
                break;
            case 4:
                emhyrRelentless();
                break;
            case 5:
                eredinWorld();
                break;
            case 6:
                redrawCard();
                break;
            case 7:
                //nothing happens
                break;
        }
    }

    void refreshImages() { //ich hoffe das klappt :P

        if(selected==0 && list.size()>0){
            imageView1.setImage(null);
            imageView2.setImage(null);
            imageView3.setImage(new Image("/assets/cards/easy/" + list.get(0).getCardName() + ".png"));
            if (list.size() > 1) imageView4.setImage(new Image("/assets/cards/easy/" + list.get(1).getCardName() + ".png"));
            else imageView1.setImage(null);
            if (list.size() > 2) imageView5.setImage(new Image("/assets/cards/easy/" + list.get(2).getCardName() + ".png"));
            else imageView1.setImage(null);

        }else if(selected==1 && list.size()>1){
            imageView1.setImage(null);
            imageView2.setImage(new Image("/assets/cards/easy/" + list.get(0).getCardName() + ".png"));
            imageView3.setImage(new Image("/assets/cards/easy/" + list.get(1).getCardName() + ".png"));
            if (list.size() > 2) imageView4.setImage(new Image("/assets/cards/easy/" + list.get(2).getCardName() + ".png"));
            else imageView1.setImage(null);
            if (list.size() > 3) imageView5.setImage(new Image("/assets/cards/easy/" + list.get(3).getCardName() + ".png"));
            else imageView1.setImage(null);

        }else if(selected>=2 && selected<list.size()-2){
            imageView1.setImage(new Image("/assets/cards/easy/" + list.get(selected-2).getCardName() + ".png"));
            imageView2.setImage(new Image("/assets/cards/easy/" + list.get(selected-1).getCardName() + ".png"));
            imageView3.setImage(new Image("/assets/cards/easy/" + list.get(selected).getCardName() + ".png"));
            imageView4.setImage(new Image("/assets/cards/easy/" + list.get(selected+1).getCardName() + ".png"));
            imageView5.setImage(new Image("/assets/cards/easy/" + list.get(selected+2).getCardName() + ".png"));

        }else if(selected==list.size()-2){
            if(list.size()>3)imageView1.setImage(new Image("/assets/cards/easy/" + list.get(selected-2).getCardName() + ".png"));
            else imageView1.setImage(null);
            imageView2.setImage(new Image("/assets/cards/easy/" + list.get(selected-1).getCardName() + ".png"));
            imageView3.setImage(new Image("/assets/cards/easy/" + list.get(selected).getCardName() + ".png"));
            imageView4.setImage(new Image("/assets/cards/easy/" + list.get(selected+1).getCardName() + ".png"));
            imageView5.setImage(null);

        }else if(selected==list.size()-1 && list.size()>2){
            imageView1.setImage(new Image("/assets/cards/easy/" + list.get(selected-2).getCardName() + ".png"));
            imageView2.setImage(new Image("/assets/cards/easy/" + list.get(selected-1).getCardName() + ".png"));
            imageView3.setImage(new Image("/assets/cards/easy/" + list.get(selected).getCardName() + ".png"));
            imageView4.setImage(null);
            imageView5.setImage(null);

        }
        else System.out.println("Error displaying the cards");
    }
}
