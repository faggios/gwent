package view.mainview;

import controller.MainController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import view.deckBuilder.DeckBuilderController;
import view.startGame.StartGameController;

import java.io.IOException;

public class MainviewController extends BorderPane {

    @FXML
    private Button playButton;

    @FXML
    private Button deckBuilderButton;

    private StackPane stackPane;
    private MainController mainController;

    public MainviewController(StackPane stackPane, MainController mainController){
        this.stackPane = stackPane;
        this.mainController = mainController;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/mainview/Mainview.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mainController.getIoController().load();
    }

    @FXML
    void deckBuilderClicked(ActionEvent event) {
        stackPane.getChildren().add(new DeckBuilderController(stackPane,mainController));
        stackPane.getChildren().remove(this);
    }

    @FXML
    void playClicked(ActionEvent event) {
        stackPane.getChildren().add(new StartGameController(mainController,stackPane));
        stackPane.getChildren().remove(this);
    }

}

