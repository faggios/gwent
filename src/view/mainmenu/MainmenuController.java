package view.mainmenu;

import controller.MainController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import view.mainview.MainviewController;
import view.pauseMenu.PauseMenuController;
import view.settings.SettingsController;

import java.io.IOException;

public class MainmenuController extends BorderPane {

    @FXML
    private StackPane stackPane;

    @FXML
    private Button settingsButton;

    public Stage primaryStage;
    private MainController mainController;

    public MainmenuController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.mainController = new MainController(stackPane);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/mainmenu/Mainmenu.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize() {
        stackPane.maxHeightProperty().bind(primaryStage.maxHeightProperty());
        stackPane.maxWidthProperty().bind(primaryStage.maxWidthProperty());
        mainController.setStackPane(stackPane);
        stackPane.getChildren().add(new MainviewController(stackPane,mainController));
    }

    @FXML
    void settingsClicked(ActionEvent event) {
        stackPane.getChildren().add(new PauseMenuController(stackPane,mainController));
    }

    /**
     * Bad hack method to write unicode texts as normal in Java.
     *
     * @param value The unicode string
     * @return Valid unicode encoded string
     */
    @SuppressWarnings("ConstantConditions")
    public static String encodeUnicode(String value) {
        return value.replace("ä", "\u00E4").replace("Ä", "\u00C4")
                .replace("ü", "\u00FC").replace("Ü", "\u00DC")
                .replace("ö", "\u00F6").replace("Ö", "\u00D6")
                .replace("ß", "\u00DF");
    }
}
