package view.scojatelFaction;
import controller.MainController;
import controller.MultiplayerController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import model.Game;

import java.io.IOException;

public class ScojatelFactionController extends BorderPane{

    @FXML
    private BorderPane root;

    @FXML
    private ImageView scojatelMessage;

    @FXML
    private Button goFirstButton;

    @FXML
    private Button letOpponentStartButton;

    private StackPane stackPane;
    private MainController mainController;
    private Game game;

    public ScojatelFactionController(StackPane stackPane, MainController mainController, Game game){
        this.mainController = mainController;
        this.stackPane = stackPane;
        this.game = game;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/scojatelFaction/ScojatelFaction.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize(){
        this.prefHeightProperty().bind(stackPane.heightProperty());
        this.prefWidthProperty().bind(stackPane.widthProperty());
    }

    @FXML
    void goFirstClicked(ActionEvent event) {
        if (MultiplayerController.isHost)
            mainController.getMultiplayerController().send(1);
        else
            mainController.getMultiplayerController().send(2);
        game.setCurrentPlayer(MultiplayerController.isHost?1:2);
        stackPane.getChildren().remove(this);
    }

    @FXML
    void letOpponentStartClicked(ActionEvent event) {
        if (!MultiplayerController.isHost)
            mainController.getMultiplayerController().send(1);
        else
            mainController.getMultiplayerController().send(2);
        game.setCurrentPlayer(MultiplayerController.isHost?2:1);
        stackPane.getChildren().remove(this);
    }

}
