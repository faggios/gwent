package view.gameboard;

import controller.MainController;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;


import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import model.*;
import view.selectCardView.SelectCardController;

import java.io.IOException;

public class GameBoardController extends BorderPane {

    @FXML
    private BorderPane borderPane;

    @FXML
    private GridPane rightSideGridPane;

    @FXML
    private ImageView imageViewEnemyDiscardPile, imageViewEnemyDrawPile, imageViewYouDiscardPile, imageViewYouDrawPile;

    @FXML
    private ImageView imageViewInspectCard,imageViewEnemyLeader, imageViewEnemyLeaderUsed, imageViewYouLeader, imageViewYouLeaderUsed,imageViewEnemyLogo, imageViewYouLogo;

    @FXML
    private Text textEnemyName, textYouName,textFactionEnemy, textFactionYou;

    @FXML
    private ImageView imageViewCardIconEnemy, imageViewCardIconYou;

    @FXML
    private Text textCardAmountEnemy, textCardAmountYou,textEnemyDrawPile,textYouDrawPile;

    @FXML
    private ImageView imageViewCrystalOneEnemy, imageViewCrystalTwoEnemy, imageViewCrystalOneYou, imageViewCrystalTwoYou;

    @FXML
    private ImageView imageViewWeatherEffects;

    @FXML
    private HBox hBoxWeatherEffects,hBoxHandcards;

    @FXML
    private ImageView imageViewWinnerEnemy, imageViewWinnerYou;

    @FXML
    private Text textTotalPointsEnemy, textTotalPointsYou;

    @FXML
    private ImageView imageViewBoostAtEnemy, imageViewBoostRaEnemy, imageViewBoostMeEnemy, imageViewBoostMeYou, imageViewBoostRaYou, imageViewBoostAtYou;

    @FXML
    private ImageView imageViewRowPointsAtEnemy, imageViewRowPointsRaEnemy, imageViewRowPointsMeEnemy, imageViewRowPointsMeYou, imageViewRowPointsRaYou, imageViewRowPointsAtYou;

    @FXML
    private Text textPointsRowAtEnemy, textPointsRowRaEnemy, textPointsRowMeEnemy, textPointsRowMeYou, textPointsRowRaYou, textPointsRowAtYou;

    @FXML
    private HBox hBoxAtCardsEnemy, hBoxRaCardsEnemy, hBoxMeCardsEnemy, hBoxMeCardsYou, hBoxRaCardsYou, hBoxAtCardsYou;

    @FXML
    private Button rowWeatherButton, rowAtEnemyButton, rowRaEnemyButton, rowMeEnemyButton, rowMeYouButton, rowRaYouButton, rowAtYouButton;

    private MainController mainController;
    private StackPane stackPane;
    private boolean isHost,leaderSelected,gameBoardLocked;
    private DoubleBinding cardSizeHeight, cardSizeWidth;

    private GameCardController selectedHandcard;

    public GameBoardController(MainController mainController, StackPane stackPane, Boolean isHost) { //TODO im view: Zahlen über drawpiles, background
        this.mainController = mainController;
        this.stackPane = stackPane;
        this.isHost = isHost;
        leaderSelected = false;
        gameBoardLocked = true;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/gameboard/GameBoard.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void toggleLock(){
        gameBoardLocked = !gameBoardLocked;
    }

    public boolean isLocked(){
        return gameBoardLocked;
    }

    @FXML
    void initialize() { //TODO listener für pasen oder button

        this.prefWidthProperty().bind(stackPane.widthProperty());
        this.prefHeightProperty().bind(stackPane.heightProperty());

        cardSizeHeight = stackPane.widthProperty().multiply(0.0625);
        cardSizeWidth = stackPane.widthProperty().multiply(0.046875);
        //-----size bindings-----

        imageViewEnemyDiscardPile.fitHeightProperty().bind(cardSizeHeight);
        imageViewEnemyDiscardPile.fitWidthProperty().bind(cardSizeWidth);
        imageViewYouDiscardPile.fitHeightProperty().bind(cardSizeHeight);
        imageViewYouDiscardPile.fitWidthProperty().bind(cardSizeWidth);
        imageViewYouDrawPile.fitHeightProperty().bind(cardSizeHeight);
        imageViewYouDrawPile.fitWidthProperty().bind(cardSizeWidth);
        imageViewEnemyDrawPile.fitHeightProperty().bind(cardSizeHeight);
        imageViewEnemyDrawPile.fitWidthProperty().bind(cardSizeWidth);
        imageViewInspectCard.fitWidthProperty().bind(rightSideGridPane.widthProperty());
        imageViewEnemyLeader.fitHeightProperty().bind(cardSizeHeight);
        imageViewEnemyLeader.fitWidthProperty().bind(cardSizeWidth);
        imageViewYouLeader.fitHeightProperty().bind(cardSizeHeight);
        imageViewYouLeader.fitWidthProperty().bind(cardSizeWidth);
        imageViewEnemyLeaderUsed.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.021));
        imageViewEnemyLeaderUsed.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.021));
        imageViewYouLeaderUsed.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.021));
        imageViewYouLeaderUsed.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.021));
        imageViewEnemyLogo.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.12));
        imageViewEnemyLogo.fitWidthProperty().bind(stackPane.heightProperty().multiply(0.12));
        imageViewYouLogo.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.12));
        imageViewYouLogo.fitWidthProperty().bind(stackPane.heightProperty().multiply(0.12));
        imageViewCardIconYou.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.014));
        imageViewCardIconYou.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.035));
        imageViewCardIconEnemy.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.014));
        imageViewCardIconEnemy.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.035));
        imageViewCrystalOneYou.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.04));
        imageViewCrystalOneYou.fitWidthProperty().bind(stackPane.heightProperty().multiply(0.04));
        imageViewCrystalOneEnemy.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.04));
        imageViewCrystalOneEnemy.fitWidthProperty().bind(stackPane.heightProperty().multiply(0.04));
        imageViewCrystalTwoYou.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.04));
        imageViewCrystalTwoYou.fitWidthProperty().bind(stackPane.heightProperty().multiply(0.04));
        imageViewCrystalTwoEnemy.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.04));
        imageViewCrystalTwoEnemy.fitWidthProperty().bind(stackPane.heightProperty().multiply(0.04));
        //imageViewWeatherEffects //TODO nachgucken im game
        //hBoxWeatherEffects //TODO nachgucken im game
        imageViewWinnerEnemy.fitWidthProperty().bind(stackPane.heightProperty().multiply(0.05));
        imageViewWinnerEnemy.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.05));
        imageViewWinnerYou.fitWidthProperty().bind(stackPane.heightProperty().multiply(0.05));
        imageViewWinnerEnemy.fitHeightProperty().bind(stackPane.heightProperty().multiply(0.05));
        hBoxHandcards.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.421));
        hBoxHandcards.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.112));
        imageViewBoostAtEnemy.fitHeightProperty().bind(cardSizeHeight);
        imageViewBoostAtEnemy.fitWidthProperty().bind(cardSizeWidth);
        imageViewBoostRaEnemy.fitHeightProperty().bind(cardSizeHeight);
        imageViewBoostRaEnemy.fitWidthProperty().bind(cardSizeWidth);
        imageViewBoostMeEnemy.fitHeightProperty().bind(cardSizeHeight);
        imageViewBoostMeEnemy.fitWidthProperty().bind(cardSizeWidth);
        imageViewBoostAtYou.fitHeightProperty().bind(cardSizeHeight);
        imageViewBoostAtYou.fitWidthProperty().bind(cardSizeWidth);
        imageViewBoostRaYou.fitHeightProperty().bind(cardSizeHeight);
        imageViewBoostRaYou.fitWidthProperty().bind(cardSizeWidth);
        imageViewBoostMeYou.fitHeightProperty().bind(cardSizeHeight);
        imageViewBoostMeYou.fitWidthProperty().bind(cardSizeWidth);
        imageViewRowPointsAtEnemy.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsRaEnemy.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsMeEnemy.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsAtEnemy.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsRaEnemy.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsMeEnemy.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsAtYou.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsRaYou.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsMeYou.fitWidthProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsAtYou.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsRaYou.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.024));
        imageViewRowPointsMeYou.fitHeightProperty().bind(stackPane.widthProperty().multiply(0.024));
        hBoxAtCardsEnemy.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.421));
        hBoxAtCardsEnemy.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.112));
        hBoxAtCardsYou.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.421));
        hBoxAtCardsYou.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.112));
        hBoxAtCardsYou.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.421));
        hBoxRaCardsEnemy.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.421));
        hBoxRaCardsEnemy.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.112));
        hBoxRaCardsYou.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.421));
        hBoxRaCardsYou.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.112));
        hBoxMeCardsEnemy.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.421));
        hBoxMeCardsEnemy.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.112));
        hBoxMeCardsYou.prefWidthProperty().bind(stackPane.widthProperty().multiply(0.421));
        hBoxMeCardsYou.prefHeightProperty().bind(stackPane.heightProperty().multiply(0.112));


        //-----other bindings-----

        //-----default texts-----
        textEnemyName.setText(mainController.getGameController().myplayer().getPlayerName());
        textYouName.setText(mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()).getPlayerName());
        textFactionEnemy.setText(mainController.getGameController().myplayer().getFaction().getName().toString());
        textFactionEnemy.setText(mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()).getFaction().getName().toString());

        //-----default images + background-----

        //imageViewWinnerEnemy //TODO + image reinsetzen
        //imageViewWinnerYou //TODO +
        imageViewYouLeaderUsed.setImage(null); //TODO
        imageViewYouLeaderUsed.setImage(null); //TODO


        //leaders
        imageViewEnemyLeader.setImage(new Image("/assets/cards/easy/" + mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()).getLeader().getName() + ".png"));
        imageViewYouLeader.setImage(new Image("/assets/cards/easy/" + mainController.getGameController().myplayer().getLeader().getName() + ".png"));

        //-----cards-----

    }

    public void refresh() {

        //cards + their points
        hBoxMeCardsYou.getChildren().removeIf(node -> node instanceof GameCardController);
        for (Card c : mainController.getGameController().myplayer().getCardListPlace(Place.MELEE))
            hBoxMeCardsYou.getChildren().add(new GameCardController(stackPane, mainController, this, true, c, false, myplayer()));

        hBoxRaCardsYou.getChildren().removeIf(node -> node instanceof GameCardController);
        for (Card c : mainController.getGameController().myplayer().getCardListPlace(Place.RANGED))
            hBoxRaCardsYou.getChildren().add(new GameCardController(stackPane, mainController, this, true, c, false, myplayer()));

        hBoxAtCardsYou.getChildren().removeIf(node -> node instanceof GameCardController);
        for (Card c : mainController.getGameController().myplayer().getCardListPlace(Place.ARTILLERY))
            hBoxAtCardsYou.getChildren().add(new GameCardController(stackPane, mainController, this, true, c, false, myplayer()));

        hBoxMeCardsEnemy.getChildren().removeIf(node -> node instanceof GameCardController);
        for (Card c : mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()).getCardListPlace(Place.MELEE))
            hBoxMeCardsEnemy.getChildren().add(new GameCardController(stackPane, mainController, this, true, c, false, enemyPlayer()));

        hBoxRaCardsEnemy.getChildren().removeIf(node -> node instanceof GameCardController);
        for (Card c : mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()).getCardListPlace(Place.RANGED))
            hBoxRaCardsEnemy.getChildren().add(new GameCardController(stackPane, mainController, this, true, c, false, enemyPlayer()));

        hBoxAtCardsEnemy.getChildren().removeIf(node -> node instanceof GameCardController);
        for (Card c : mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()).getCardListPlace(Place.ARTILLERY))
            hBoxAtCardsEnemy.getChildren().add(new GameCardController(stackPane, mainController, this, true, c, false, enemyPlayer()));

        hBoxHandcards.getChildren().removeIf(node -> node instanceof GameCardController);
        for (Card c : mainController.getGameController().myplayer().getHandcards())
            hBoxHandcards.getChildren().add(new GameCardController(stackPane, mainController, this, true, c, true, myplayer()));

        //weathercards
        hBoxWeatherEffects.getChildren().clear();
        for (EffectType e : myplayer().getRowEffects().get(Place.MELEE))
            if (e.equals(EffectType.WEATHER))
                hBoxWeatherEffects.getChildren().add(new GameCardController(stackPane, mainController, this, false, Card.buildCard(CardName.Biting_Frost), false, myplayer()));
        for (EffectType e : myplayer().getRowEffects().get(Place.RANGED))
            if (e.equals(EffectType.WEATHER))
                hBoxWeatherEffects.getChildren().add(new GameCardController(stackPane, mainController, this, false, Card.buildCard(CardName.Impenetrable_Fog), false, myplayer()));
        for (EffectType e : myplayer().getRowEffects().get(Place.ARTILLERY))
            if (e.equals(EffectType.WEATHER))
                hBoxWeatherEffects.getChildren().add(new GameCardController(stackPane, mainController, this, false, Card.buildCard(CardName.Torrential_Rain), false, myplayer()));

        //row points
        textPointsRowAtEnemy.setText(Integer.toString(mainController.getPointController().calculateCombatPoints(mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()), Place.ARTILLERY)));
        textPointsRowRaEnemy.setText(Integer.toString(mainController.getPointController().calculateCombatPoints(mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()), Place.RANGED)));
        textPointsRowMeEnemy.setText(Integer.toString(mainController.getPointController().calculateCombatPoints(mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()), Place.MELEE)));
        textPointsRowAtYou.setText(Integer.toString(mainController.getPointController().calculateCombatPoints(mainController.getGameController().myplayer(), Place.ARTILLERY)));
        textPointsRowRaYou.setText(Integer.toString(mainController.getPointController().calculateCombatPoints(mainController.getGameController().myplayer(), Place.RANGED)));
        textPointsRowMeYou.setText(Integer.toString(mainController.getPointController().calculateCombatPoints(mainController.getGameController().myplayer(), Place.MELEE)));

        int enemyPoints = mainController.getPointController().calculateCombatPoints(mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()));
        textTotalPointsEnemy.setText(Integer.toString(enemyPoints));
        int myPoints = mainController.getPointController().calculateCombatPoints(mainController.getGameController().myplayer());
        textTotalPointsYou.setText(Integer.toString(myPoints));
        imageViewWinnerEnemy.setVisible(enemyPoints >= myPoints);
        imageViewWinnerYou.setVisible(enemyPoints <= myPoints);


        //leaderused or no
        imageViewYouLeaderUsed.setImage(null);//TODO pfad angeben
        imageViewEnemyLeaderUsed.setImage(null);//TODO pfad angeben
        imageViewYouLeaderUsed.setVisible(!myplayer().getLeader().isUsed());
        imageViewEnemyLeaderUsed.setVisible(!mainController.getPlayerController().getOtherPlayer(myplayer()).getLeader().isUsed());
        textEnemyDrawPile.setText(""+mainController.getPlayerController().getOtherPlayer(myplayer()).getDrawPile().size());
        textYouDrawPile.setText(""+myplayer().getDrawPile().size());

        textCardAmountEnemy.setText(Integer.toString(mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer()).getHandcards().size()));
        textCardAmountYou.setText(Integer.toString(mainController.getGameController().myplayer().getHandcards().size()));

        //whos turn
    }

    public Player myplayer() {
        return mainController.getGameController().myplayer();
    }

    public Player enemyPlayer() {
        return mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer());
    }

    public boolean isMyTurn() {
        return mainController.getGameController().getCurrentPlayer().equals(mainController.getGameController().myplayer());
    }

    @FXML
    void imageViewEnemyDiscardPilePressed(MouseEvent event) {
        Player enemy = mainController.getPlayerController().getOtherPlayer(mainController.getGameController().myplayer());
        stackPane.getChildren().add(new SelectCardController(stackPane, mainController, 7, enemy, enemy.getDiscardPile(), true, null));
    }

    @FXML
    void imageViewEnemyDrawPilePressed(MouseEvent event) {

    }

    @FXML
    void imageViewYouDiscardPilePressed(MouseEvent event) {
        Player you = mainController.getGameController().myplayer();
        stackPane.getChildren().add(new SelectCardController(stackPane, mainController, 7, you, you.getDiscardPile(), true, null));
    }

    @FXML
    void imageViewYouDrawPilePressed(MouseEvent event) {

    }

    @FXML
    void imageViewYouLeaderClicked(MouseEvent event) {
        if (isMyTurn() && !mainController.getGameController().myplayer().getLeader().isUsed()) {
            if (!leaderSelected) {
                resetSelectedCard();
                leaderSelected = true;
            } else{
                resetSelectedCard();
            }
            setSelectedImage();
        }
    }

    public DoubleBinding getCardHeight() {
        return cardSizeHeight;
    }

    public DoubleBinding getCardWidth() {
        return cardSizeWidth;
    }

    public GameCardController getSelectedHandcard() {
        return selectedHandcard;
    }

    public void resetSelectedCard() {
        if (selectedHandcard != null)
            selectedHandcard.setSelected(false);
        selectedHandcard = null;
        if (leaderSelected) {
            leaderSelected = false;
        }
    }

    public boolean setSelectedHandcard(GameCardController selectedHandcard) {
        if (!isMyTurn()) return false;
        if (selectedHandcard.equals(this.selectedHandcard)) {
            resetSelectedCard();
            setSelectedImage();
            return false;
        } else {
            if (this.selectedHandcard != null || leaderSelected)
                resetSelectedCard();
            this.selectedHandcard = selectedHandcard;

            if (selectedHandcard.getCard().getTypes().contains(EffectType.DECOY)) {
                hBoxAtCardsYou.toFront();
                hBoxRaCardsYou.toFront();
                hBoxMeCardsYou.toFront();
                rowAtEnemyButton.toFront();
                rowRaEnemyButton.toFront();
                rowMeEnemyButton.toFront();
                rowWeatherButton.toFront();
            } else {
                rowAtEnemyButton.toFront();
                rowAtYouButton.toFront();
                rowRaEnemyButton.toFront();
                rowRaYouButton.toFront();
                rowMeEnemyButton.toFront();
                rowMeYouButton.toFront();
                rowWeatherButton.toFront();
            }

        }
        setSelectedImage();
        return true;
    }

    public void setSelectedImage() {
        if (selectedHandcard != null) {
            imageViewInspectCard.setImage(new Image("/assets/cards/easy/" + selectedHandcard.getCard().getCardName().toString() + ".png"));
        } else if (leaderSelected) {
            imageViewInspectCard.setImage(new Image("/assets/cards/easy/" + myplayer().getLeader().getName().toString() + ".png"));
        } else {
            imageViewInspectCard.setImage(new Image(""));
        }
    }

    public void hboxToFront() {
        hBoxWeatherEffects.toFront();
        hBoxMeCardsYou.toFront();
        hBoxRaCardsYou.toFront();
        hBoxRaCardsEnemy.toFront();
        hBoxMeCardsEnemy.toFront();
        hBoxAtCardsEnemy.toFront();
        hBoxAtCardsYou.toFront();
    }

    public void pointedCard(Card card, GameCardController gameCardController) {
        if (selectedHandcard != null) {
            mainController.getPlayerController().playCard(myplayer(), gameCardController.getPlayer(), selectedHandcard.getCard(), card, null, myplayer().getHandcards(), false);
            selectedHandcard = null;
        }
    }

    public void pointedPlace(Place place, boolean you) {
        if (selectedHandcard != null) {
            if (you)
                mainController.getPlayerController().playCard(myplayer(), selectedHandcard.getPlayer(), selectedHandcard.getCard(), null, place, myplayer().getHandcards(), false);
            else
                mainController.getPlayerController().playCard(myplayer(), mainController.getPlayerController().getOtherPlayer(selectedHandcard.getPlayer()), selectedHandcard.getCard(), null, place, myplayer().getHandcards(), false);
            selectedHandcard = null;
        }
    }

    @FXML
    void rowAtEnemyClicked(MouseEvent event) {
        if(gameBoardLocked)return;
        if (selectedHandcard == null && !leaderSelected) return;
        if (!leaderSelected && (selectedHandcard.getCard().getCardName().equals(CardName.Scorch) || selectedHandcard.getCard().getTypes().contains(EffectType.WEATHER) || selectedHandcard.getCard().getTypes().contains(EffectType.CLEARWEATHER) || (selectedHandcard.getCard().getTypes().contains(EffectType.SPY) && selectedHandcard.getCard().getPlace(Place.ARTILLERY)))) {
            pointedPlace(Place.ARTILLERY, false);
            hboxToFront();
            refresh();
        } else if (leaderSelected) {
            mainController.getPlayerController().playLeader(myplayer(),myplayer().getLeader(),null,null,false);
            resetSelectedCard();
            refresh();
        }
    }

    @FXML
    void rowAtYouClicked(MouseEvent event) {
        if(gameBoardLocked)return;
        if (selectedHandcard == null && !leaderSelected) return;
        if (!leaderSelected && !selectedHandcard.getCard().getTypes().contains(EffectType.SPY) && !selectedHandcard.getCard().getTypes().contains(EffectType.DECOY) && selectedHandcard.getCard().getPlace(Place.ARTILLERY)) {
            pointedPlace(Place.ARTILLERY, true);
            hboxToFront();
            refresh();
        } else if (leaderSelected) {
            mainController.getGameController().playLeader();
            resetSelectedCard();
            refresh();
        }
    }

    @FXML
    void rowMeEnemyClicked(MouseEvent event) {
        if(gameBoardLocked)return;
        if (selectedHandcard == null && !leaderSelected) return;
        if (!leaderSelected && (selectedHandcard.getCard().getCardName().equals(CardName.Scorch) || selectedHandcard.getCard().getTypes().contains(EffectType.WEATHER) || selectedHandcard.getCard().getTypes().contains(EffectType.CLEARWEATHER) || (selectedHandcard.getCard().getTypes().contains(EffectType.SPY) && selectedHandcard.getCard().getPlace(Place.MELEE)))) {
            pointedPlace(Place.MELEE, false);
            hboxToFront();
            refresh();
        } else if (leaderSelected) {
            mainController.getGameController().playLeader();
            resetSelectedCard();
            refresh();
        }
    }

    @FXML
    void rowMeYouClicked(MouseEvent event) {
        if(gameBoardLocked)return;
        if (selectedHandcard == null && !leaderSelected) return;
        if (!leaderSelected && !selectedHandcard.getCard().getTypes().contains(EffectType.SPY) && !selectedHandcard.getCard().getTypes().contains(EffectType.DECOY) && selectedHandcard.getCard().getPlace(Place.MELEE)) {
            pointedPlace(Place.MELEE, true);
            hboxToFront();
            refresh();
        } else if (leaderSelected) {
            mainController.getGameController().playLeader();
            resetSelectedCard();
            refresh();
        }
    }

    @FXML
    void rowRaEnemyClicked(MouseEvent event) {
        if(gameBoardLocked)return;
        if (selectedHandcard == null && !leaderSelected) return;
        if (!leaderSelected && (selectedHandcard.getCard().getCardName().equals(CardName.Scorch) || selectedHandcard.getCard().getTypes().contains(EffectType.WEATHER) || selectedHandcard.getCard().getTypes().contains(EffectType.CLEARWEATHER) || (selectedHandcard.getCard().getTypes().contains(EffectType.SPY) && selectedHandcard.getCard().getPlace(Place.RANGED)))) {
            pointedPlace(Place.RANGED, false);
            hboxToFront();
            refresh();
        } else if (leaderSelected) {
            mainController.getGameController().playLeader();
            resetSelectedCard();
            refresh();
        }
    }

    @FXML
    void rowRaYouClicked(MouseEvent event) {
        if(gameBoardLocked)return;
        if (selectedHandcard == null && !leaderSelected) return;
        if (!leaderSelected && !selectedHandcard.getCard().getTypes().contains(EffectType.SPY) && !selectedHandcard.getCard().getTypes().contains(EffectType.DECOY) && selectedHandcard.getCard().getPlace(Place.RANGED)) {
            pointedPlace(Place.RANGED, true);
            hboxToFront();
            refresh();
        } else if (leaderSelected) {
            mainController.getGameController().playLeader();
            resetSelectedCard();
            refresh();
        }
    }

    @FXML
    void rowWeatherClicked(MouseEvent event) {
        if(gameBoardLocked)return;
        if (selectedHandcard == null && !leaderSelected) return;
        if (!leaderSelected && (selectedHandcard.getCard().getTypes().contains(EffectType.WEATHER) || selectedHandcard.getCard().getTypes().contains(EffectType.CLEARWEATHER))) {
            pointedPlace(Place.MELEE, true);
            hboxToFront();
            refresh();
        } else if (leaderSelected) {
            mainController.getGameController().playLeader();
            resetSelectedCard();
            refresh();
        }
    }

}
