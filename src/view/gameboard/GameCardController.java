package view.gameboard;

import com.pengrad.telegrambot.request.DeleteChatStickerSet;
import controller.MainController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.Card;
import model.Player;

import java.io.IOException;

public class GameCardController extends BorderPane {

    @FXML
    private GridPane cardGridPane;

    @FXML
    private Text cardPointText;

    @FXML
    private ImageView cardImageView;

    private StackPane stackPane;
    private MainController mainController;
    private GameBoardController gameBoardController;
    private boolean hasPoints, isHandcard, isSelected;
    private Card card;
    private Player player;

    public GameCardController(StackPane stackPane, MainController mainController, GameBoardController gameBoardController, boolean hasPoints, Card card, boolean isHandcard, Player player) {
        this.stackPane = stackPane;
        this.mainController = mainController;
        this.gameBoardController = gameBoardController;
        this.hasPoints = hasPoints;
        this.card = card;
        this.isHandcard = isHandcard;
        this.isSelected = false;
        this.player = player;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/gameboard/GameCard.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize() {
        if (hasPoints) {
            cardPointText.setText(Integer.toString(card.getPoints()));
        } else {
            cardPointText.setText("");
        }
        cardImageView.setImage(new Image("/assets/cards/easy/" + card.getCardName() + ".png"));
        this.prefHeightProperty().bind(gameBoardController.getCardHeight());
        this.prefWidthProperty().bind(gameBoardController.getCardWidth());
        this.cardGridPane.prefHeightProperty().bind(gameBoardController.getCardHeight());
        this.cardGridPane.prefWidthProperty().bind(gameBoardController.getCardWidth());
    }

    @FXML
    void cardClicked(MouseEvent event) { //TODO
        if(gameBoardController.isLocked())return;
        if (gameBoardController.setSelectedHandcard(this)&&isHandcard)
            isSelected = true;
        else if (isHandcard)
            isSelected = false;
        else{
            gameBoardController.pointedCard(card,this);
        }
    }

    public Player getPlayer() { return player; }

    public void setPlayer(Player player) { this.player = player; }

    public void setSelected(boolean b) { //TODO view aktualisieren
        isSelected = b;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public void setCardPointText(int points) {
        cardPointText.setText(Integer.toString(points));
        refreshColor();
    }

    public void changeCardPointText(int changePoint) {
        cardPointText.setText(Integer.parseInt(cardPointText.getText()) + Integer.toString(changePoint));
        refreshColor();
    }

    public int getCardPointText() {
        return Integer.parseInt(cardPointText.getText());
    }

    public void refreshPoints() {
        cardPointText.setText(Integer.toString(card.getPoints()));
        refreshColor();
    }

    void refreshColor() {
        if (card.getPoints() > card.getDefaultPoints())
            cardPointText.setFill(new Color(0, 255, 0, 1));
        else if (card.getPoints() < card.getDefaultPoints())
            cardPointText.setFill(new Color(255, 0, 0, 1));
        else
            cardPointText.setFill(new Color(0, 0, 0, 1));
    }
}
